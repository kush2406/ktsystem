﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keygenerator
{
    static class nconvert
    {
        public static string getcode(UInt64 cd)
        {
            string bin = tobinary(cd);
            string hex = tohexa(bin);
            return hex;
        }
        private static string tobinary(UInt64 cd)
        {
            string ret = "";
            while (cd != 0)
            { 
            UInt64 te=cd%2;
            ret = te.ToString() + ret;
            cd = cd / 2;
            }
            return ret;
        }
        private static string tohexa(string bin)
        {
            string ret = "";
            int len = bin.Length;
            if (len % 5 != 0)
            {
                int te = len / 5;
                int tee = len - te * 5;
                tee = 5 - tee;
                for (int i = 1; i <= tee; i++)
                {
                    bin = "0" + bin;
                }
            }
            while (bin.Length != 0)
            {
                string fiv = bin.Substring(0, 5);
                bin = bin.Substring(5);
                ret = ret + gethex(fiv);
            }
            
            return ret;
        }
        private static string gethex(string st)
        {
            string stt = "";
            switch (st)
            {
                case "00000":
                    stt = "2";
                    break;
                case "00001":
                    stt = "3";
                    break;
                case "00010":
                    stt = "4";
                    break;
                case "00011":
                    stt = "5";
                    break;
                case "00100":
                    stt = "6";
                    break;
                case "00101":
                    stt = "7";
                    break;
                case "00110":
                    stt = "8";
                    break;
                case "00111":
                    stt = "9";
                    break;
                case "01000":
                    stt = "A";
                    break;
                case "01001":
                    stt = "B";
                    break;
                case "01010":
                    stt = "C";
                    break;
                case "01011":
                    stt = "D";
                    break;
                case "01100":
                    stt = "E";
                    break;
                case "01101":
                    stt = "F";
                    break;
                case "01110":
                    stt = "G";
                    break;
                case "01111":
                    stt = "H";
                    break;
                case "10000":
                    stt = "J";
                    break;
                case "10001":
                    stt = "K";
                    break;
                case "10010":
                    stt = "L";
                    break;
                case "10011":
                    stt = "M";
                    break;
                case "10100":
                    stt = "N";
                    break;
                case "10101":
                    stt = "P";
                    break;
                case "10110":
                    stt = "Q";
                    break;
                case "10111":
                    stt = "R";
                    break;
                case "11000":
                    stt = "S";
                    break;
                case "11001":
                    stt = "T";
                    break;
                case "11010":
                    stt = "U";
                    break;
                case "11011":
                    stt = "V";
                    break;
                case "11100":
                    stt = "W";
                    break;
                case "11101":
                    stt = "X";
                    break;
                case "11110":
                    stt = "Y";
                    break;
                case "11111":
                    stt = "Z";
                    break;
            }
            return stt;
        }
        public static int toodec(string st)
        {
            string bin = toobinary(st);
            int ret = 0;
            int count=0;
            while(bin.Length != 0)
            {
                int ik=System.Convert.ToInt16(bin[bin.Length-1]);
                ik = ik - 48;
                ret=ret+(ik*(int)(System.Math.Pow((double)2,(double)count)));
                bin = bin.Substring(0,bin.Length-1);
                count++;
            }
            return ret;
        }
        private static string toobinary( string st)
        {
            string ret = "";
            while (st.Length != 0)
            {
                string te = st[0].ToString();
                ret = ret + bincode(te);
                st = st.Substring(1);
            }
            return ret;
        }
        private static string bincode(string st)
        {
            string stt="";
            switch (st)
            {
                case "2":
                    stt = "00000";
                    break;
                case "3":
                    stt = "00001";
                    break;
                case "4":
                    stt = "00010";
                    break;
                case "5":
                    stt = "00011";
                    break;
                case "6":
                    stt = "00100";
                    break;
                case "7":
                    stt = "00101";
                    break;
                case "8":
                    stt = "00110";
                    break;
                case "9":
                    stt = "00111";
                    break;
                case "A":
                    stt = "01000";
                    break;
                case "B":
                    stt = "01001";
                    break;
                case "C":
                    stt = "01010";
                    break;
                case "D":
                    stt = "01011";
                    break;
                case "E":
                    stt = "01100";
                    break;
                case "F":
                    stt = "01101";
                    break;
                case "G":
                    stt = "01110";
                    break;
                case "H":
                    stt = "01111";
                    break;
                case "J":
                    stt = "10000";
                    break;
                case "K":
                    stt = "10001";
                    break;
                case "L":
                    stt = "10010";
                    break;
                case "M":
                    stt = "10011";
                    break;
                case "N":
                    stt = "10100";
                    break;
                case "P":
                    stt = "10101";
                    break;
                case "Q":
                    stt = "10110";
                    break;
                case "R":
                    stt = "10111";
                    break;
                case "S":
                    stt = "11000";
                    break;
                case "T":
                    stt = "11001";
                    break;
                case "U":
                    stt = "11010";
                    break;
                case "V":
                    stt = "11011";
                    break;
                case "W":
                    stt = "11100";
                    break;
                case "X":
                    stt = "11101";
                    break;
                case "Y":
                    stt = "11110";
                    break;
                case "Z":
                    stt = "11111";
                    break;
            }
            return stt;
        }
    }
}
