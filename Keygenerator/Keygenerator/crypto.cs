﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keygenerator
{
    static class crypto
    {
        public static UInt64 EncryptString(UInt64 me)
        {
            Random r = new Random();
            int no = r.Next(9);
            UInt64 ret = 0;
            while (me != 0)
            {
                UInt64 te = me % 10;
                UInt64 te1 = te + (UInt64)no;
                if (te1 >= 10)
                {
                    te1 = te1 % 10;
                }
                ret = ret * 10 + (UInt64)te1;
                me = me / 10;
            }
            ret = ret * 10 + (UInt64)no;
            return ret;
        }

        public static UInt64 DecryptString(UInt64 me)
        {
            UInt64 no = me % 10;
            me = me / 10;
            UInt64 ret = 0;
            while (me != 0)
            {
                UInt64 te = me % 10;
                if(te<no)
                {
                te=te+10;
                }
                UInt64 te1 = te - no;
                ret = ret * 10 + (UInt64)te1;
                me = me / 10;
            }
            return ret;
        }
    }
}
