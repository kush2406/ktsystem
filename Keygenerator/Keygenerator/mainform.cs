﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Keygenerator
{
    public partial class mainform : Form
    {
        public mainform()
        {
            InitializeComponent();
        }

        private void gen_Click(object sender, EventArgs e)
        {
            if (mi.Text == "")
            {
                MessageBox.Show("Please Enter Machine Id");
            }
            else
            {
                if (date.Value.Date <= DateTime.Now.Date)
                {
                    MessageBox.Show("Plesect Select Valid Date");
                }
                else
                {
                    string macid = mi.Text;
                    int hash = (int)macid.GetHashCode();
                    if (hash < 0)
                    {
                        hash = hash * -1;
                    }
                    UInt64 tpk = ((UInt64)hash * 100000000 + (UInt64)(date.Value.Year * 10000 + date.Value.Month * 100 + date.Value.Day));
                    UInt64 tpkk = crypto.EncryptString(tpk);
                    string productkey = nconvert.getcode(tpkk);
                    //productkey = crypto.EncryptString(productkey, "1");
                    pk.Text = productkey;
                }
            }
        }
    }
}
