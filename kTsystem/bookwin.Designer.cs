﻿namespace kTsystem
{
    partial class bookwin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTicketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTicketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectTicketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.websiteConditionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.worstToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectFetchTrainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editPaymentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEditCardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectProxyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editPNRIssueTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browserBehaviourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeBrowserAfterPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.geneatePNRInBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.alwaysOnTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.removeProductKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(126, 257);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(257, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "For More Information Click On Help";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(90, 131);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(317, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "To Edit Old Tickets Click on File->Edit Ticket";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(327, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "To Save New Ticket Click on File->New Ticket";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 194);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "To Start Booking Ticket Click on File->Select Ticket";
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip.Size = new System.Drawing.Size(482, 35);
            this.menuStrip.TabIndex = 7;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTicketToolStripMenuItem,
            this.editTicketToolStripMenuItem,
            this.selectTicketToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newTicketToolStripMenuItem
            // 
            this.newTicketToolStripMenuItem.Name = "newTicketToolStripMenuItem";
            this.newTicketToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.newTicketToolStripMenuItem.Text = "New Ticket";
            this.newTicketToolStripMenuItem.Click += new System.EventHandler(this.newTicketToolStripMenuItem_Click);
            // 
            // editTicketToolStripMenuItem
            // 
            this.editTicketToolStripMenuItem.Name = "editTicketToolStripMenuItem";
            this.editTicketToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.editTicketToolStripMenuItem.Text = "Edit Ticket";
            this.editTicketToolStripMenuItem.Click += new System.EventHandler(this.editTicketToolStripMenuItem_Click);
            // 
            // selectTicketToolStripMenuItem
            // 
            this.selectTicketToolStripMenuItem.Name = "selectTicketToolStripMenuItem";
            this.selectTicketToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.selectTicketToolStripMenuItem.Text = "Select Ticket";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(208, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.websiteConditionsToolStripMenuItem,
            this.selectFetchTrainToolStripMenuItem,
            this.editPaymentDetailsToolStripMenuItem,
            this.addEditCardsToolStripMenuItem,
            this.selectProxyToolStripMenuItem,
            this.editPNRIssueTimeToolStripMenuItem,
            this.browserBehaviourToolStripMenuItem,
            this.toolStripSeparator2,
            this.alwaysOnTopToolStripMenuItem,
            this.toolStripSeparator3,
            this.removeProductKeyToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(54, 29);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // websiteConditionsToolStripMenuItem
            // 
            this.websiteConditionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goodToolStripMenuItem,
            this.averageToolStripMenuItem,
            this.worstToolStripMenuItem});
            this.websiteConditionsToolStripMenuItem.Name = "websiteConditionsToolStripMenuItem";
            this.websiteConditionsToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.websiteConditionsToolStripMenuItem.Text = "Website Conditions";
            // 
            // goodToolStripMenuItem
            // 
            this.goodToolStripMenuItem.Name = "goodToolStripMenuItem";
            this.goodToolStripMenuItem.Size = new System.Drawing.Size(171, 30);
            this.goodToolStripMenuItem.Text = ">> Good";
            // 
            // averageToolStripMenuItem
            // 
            this.averageToolStripMenuItem.Name = "averageToolStripMenuItem";
            this.averageToolStripMenuItem.Size = new System.Drawing.Size(171, 30);
            this.averageToolStripMenuItem.Text = "Average";
            // 
            // worstToolStripMenuItem
            // 
            this.worstToolStripMenuItem.Name = "worstToolStripMenuItem";
            this.worstToolStripMenuItem.Size = new System.Drawing.Size(171, 30);
            this.worstToolStripMenuItem.Text = "Worst";
            // 
            // selectFetchTrainToolStripMenuItem
            // 
            this.selectFetchTrainToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.oldToolStripMenuItem});
            this.selectFetchTrainToolStripMenuItem.Name = "selectFetchTrainToolStripMenuItem";
            this.selectFetchTrainToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.selectFetchTrainToolStripMenuItem.Text = "Select Fetch Train Type";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(132, 30);
            this.newToolStripMenuItem.Text = "New";
            // 
            // oldToolStripMenuItem
            // 
            this.oldToolStripMenuItem.Name = "oldToolStripMenuItem";
            this.oldToolStripMenuItem.Size = new System.Drawing.Size(132, 30);
            this.oldToolStripMenuItem.Text = "Old";
            // 
            // editPaymentDetailsToolStripMenuItem
            // 
            this.editPaymentDetailsToolStripMenuItem.Name = "editPaymentDetailsToolStripMenuItem";
            this.editPaymentDetailsToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.editPaymentDetailsToolStripMenuItem.Text = "Edit Payment Details";
            // 
            // addEditCardsToolStripMenuItem
            // 
            this.addEditCardsToolStripMenuItem.Name = "addEditCardsToolStripMenuItem";
            this.addEditCardsToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.addEditCardsToolStripMenuItem.Text = "Add/Edit Cards";
            // 
            // selectProxyToolStripMenuItem
            // 
            this.selectProxyToolStripMenuItem.Name = "selectProxyToolStripMenuItem";
            this.selectProxyToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.selectProxyToolStripMenuItem.Text = "Select Proxy";
            // 
            // editPNRIssueTimeToolStripMenuItem
            // 
            this.editPNRIssueTimeToolStripMenuItem.Name = "editPNRIssueTimeToolStripMenuItem";
            this.editPNRIssueTimeToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.editPNRIssueTimeToolStripMenuItem.Text = "Edit Time";
            // 
            // browserBehaviourToolStripMenuItem
            // 
            this.browserBehaviourToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeBrowserAfterPaymentToolStripMenuItem,
            this.geneatePNRInBrowserToolStripMenuItem});
            this.browserBehaviourToolStripMenuItem.Name = "browserBehaviourToolStripMenuItem";
            this.browserBehaviourToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.browserBehaviourToolStripMenuItem.Text = "Browser Behaviour";
            // 
            // closeBrowserAfterPaymentToolStripMenuItem
            // 
            this.closeBrowserAfterPaymentToolStripMenuItem.Name = "closeBrowserAfterPaymentToolStripMenuItem";
            this.closeBrowserAfterPaymentToolStripMenuItem.Size = new System.Drawing.Size(329, 30);
            this.closeBrowserAfterPaymentToolStripMenuItem.Text = ">> Generate PNR in Software";
            // 
            // geneatePNRInBrowserToolStripMenuItem
            // 
            this.geneatePNRInBrowserToolStripMenuItem.Name = "geneatePNRInBrowserToolStripMenuItem";
            this.geneatePNRInBrowserToolStripMenuItem.Size = new System.Drawing.Size(329, 30);
            this.geneatePNRInBrowserToolStripMenuItem.Text = "Geneate PNR in Browser";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(270, 6);
            // 
            // alwaysOnTopToolStripMenuItem
            // 
            this.alwaysOnTopToolStripMenuItem.Name = "alwaysOnTopToolStripMenuItem";
            this.alwaysOnTopToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.alwaysOnTopToolStripMenuItem.Text = "Always on Top";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(270, 6);
            // 
            // removeProductKeyToolStripMenuItem
            // 
            this.removeProductKeyToolStripMenuItem.Name = "removeProductKeyToolStripMenuItem";
            this.removeProductKeyToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.removeProductKeyToolStripMenuItem.Text = "Remove Product Key";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.getHelpToolStripMenuItem,
            this.aboutProductToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(61, 29);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // getHelpToolStripMenuItem
            // 
            this.getHelpToolStripMenuItem.Name = "getHelpToolStripMenuItem";
            this.getHelpToolStripMenuItem.Size = new System.Drawing.Size(214, 30);
            this.getHelpToolStripMenuItem.Text = "Get Help";
            // 
            // aboutProductToolStripMenuItem
            // 
            this.aboutProductToolStripMenuItem.Name = "aboutProductToolStripMenuItem";
            this.aboutProductToolStripMenuItem.Size = new System.Drawing.Size(214, 30);
            this.aboutProductToolStripMenuItem.Text = "About Product";
            // 
            // bookwin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 363);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "bookwin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "bookwin";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTicketToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editTicketToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectTicketToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem websiteConditionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem worstToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectFetchTrainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editPaymentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEditCardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectProxyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editPNRIssueTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem browserBehaviourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeBrowserAfterPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem geneatePNRInBrowserToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem alwaysOnTopToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem removeProductKeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutProductToolStripMenuItem;
    }
}