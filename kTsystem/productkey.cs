﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using System.Windows.Forms;
using System.IO;

namespace kTsystem
{
     static class productkey
    {
         public static Boolean verify(string st)
        {
            try
            {
                //ttime t = new ttime();
                int date = DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day;//t.settime();
                staticstore.date = date;
                staticstore.chkdate = DateTime.Now.ToString("d-MMM-yyyy");
                MessageBox.Show("License Expiry Date: " + productkey.date(st));
                if (date <= productkey.date(st) && productkey.date(st).ToString().Length == 8 && productkey.date(st).ToString().Substring(0, 3) == "201")
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey1.cs", e.Message);
                return false;
            }
        }
        static public int date(string st)
        {
            try
            {
                UInt64 dec = nconvert.toodec(st);
                dec = crypto.DecryptString(dec);
                int hash = genrate().GetHashCode();
                if (hash < 0)
                {
                    hash = hash * -1;
                }
                int date = (int)(dec - (UInt64)hash * 100000000);
                return date;
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey2.cs", e.Message);
                return 0;
            }
        }
        static public string genrate()
        {
            try
            {
                //string drive = Path.GetDirectoryName(Application.ExecutablePath).Substring(0, 1);
                string drive = "F";///////////remove after installation
                string a = (getkey(drive, "PNPDeviceID"));
                string b = (getkey(drive, "Caption"));
                string c = (getkey(drive, "Size"));
                if ((a == null || a == "") && (b == null || b == "") && (c == null || c == ""))
                {
                    MessageBox.Show("Please Check Yours Installation..");
                    return ("");
                }
                else
                {
                    int aa = (int)a.GetHashCode();
                    int bb = (int)b.GetHashCode();
                    int cc = (int)c.GetHashCode();
                    if (aa < 0) aa = aa * -1;
                    if (bb < 0) bb = bb * -1;
                    if (cc < 0) cc = cc * -1;
                    string aaa = nconvert.getcode((UInt64)aa);
                    string bbb = nconvert.getcode((UInt64)bb);
                    string ccc = nconvert.getcode((UInt64)cc);
                    aaa = "H" + aaa;
                    bbb = "P" + bbb;
                    ccc = "D" + ccc;
                    return (aaa + "-" + bbb + "-" + ccc);
                }
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey3.cs", e.Message);
                return "";
            }
        }
        /*
        private static string getid(string drive, string key)
        {
            string ret = "";
            try
            {
                if (drive == "" || drive == null)
                {
                    throw new Exception();
                }
                ManagementObject disk = new ManagementObject("Win32_LogicalDisk.DeviceID=\"" + drive + ":\"");
                disk.Get();
                foreach (PropertyData pc in disk.Properties)
                {
                    try
                    {
                        MessageBox.Show(pc.Name.ToString() + "=" + pc.Value.ToString());
                    }
                    catch
                    {
                        MessageBox.Show(pc.Name.ToString());
                    }
                }
                
                ret = disk[key].ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show("There was some error in generating MI-" + e.Message + "..Contact Admin");
            }
            return ret;
        }
        */











        static string _serialNumber;
        static string _driveLetter;
        static string type;

        public static string getkey(string driveLetter, string typ)
        {
            try
            {
                type = typ;
                _driveLetter = driveLetter.ToUpper();

                if (!_driveLetter.Contains(":"))
                {
                    _driveLetter += ":";
                }

                matchDriveLetterWithSerial();

                return _serialNumber;
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey4.cs", e.Message);
                return "";
            }
        }

        public static void matchDriveLetterWithSerial()
        {
            try
            {
                string[] diskArray;
                string driveNumber;
                string driveLetter;

                ManagementObjectSearcher searcher1 = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition");
                foreach (ManagementObject dm in searcher1.Get())
                {
                    diskArray = null;
                    driveLetter = getValueInQuotes(dm["Dependent"].ToString());
                    diskArray = getValueInQuotes(dm["Antecedent"].ToString()).Split(',');
                    driveNumber = diskArray[0].Remove(0, 6).Trim();
                    if (driveLetter == _driveLetter)
                    {
                        /* This is where we get the drive serial */
                        ManagementObjectSearcher disks = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                        foreach (ManagementObject disk in disks.Get())
                        {

                            if (disk["Name"].ToString() == ("\\\\.\\PHYSICALDRIVE" + driveNumber) & disk["InterfaceType"].ToString() == "USB")
                            {
                                switch (type)
                                {
                                    case "PNPDeviceID":
                                        _serialNumber = parseSerialFromDeviceID(disk["PNPDeviceID"].ToString());
                                        break;
                                    case "Caption":
                                        _serialNumber = disk["Caption"].ToString();
                                        break;
                                    case "Size":
                                        _serialNumber = disk["Size"].ToString();
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey5.cs", e.Message);
            }
        }

        public static string parseSerialFromDeviceID(string deviceId)
        {
            try
            {
                string[] splitDeviceId = deviceId.Split('\\');
                string[] serialArray;
                string serial;
                int arrayLen = splitDeviceId.Length - 1;

                serialArray = splitDeviceId[arrayLen].Split('&');
                serial = serialArray[0];

                return serial;
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey6.cs", e.Message);
                return "";
            }
        }

        public static string getValueInQuotes(string inValue)
        {
            try
            {
                string parsedValue = "";

                int posFoundStart = 0;
                int posFoundEnd = 0;

                posFoundStart = inValue.IndexOf("\"");
                posFoundEnd = inValue.IndexOf("\"", posFoundStart + 1);

                parsedValue = inValue.Substring(posFoundStart + 1, (posFoundEnd - posFoundStart) - 1);

                return parsedValue;
            }
            catch (Exception e)
            {
                log.store("default", "Errorinnproductkey7.cs", e.Message);
                return "";
            }
        }

    }
}
