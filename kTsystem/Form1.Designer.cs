﻿namespace kTsystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainp = new System.Windows.Forms.Panel();
            this.wait = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.mainp.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::kTsystem.Properties.Resources.loading;
            this.pictureBox1.Location = new System.Drawing.Point(18, 5);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(218, 216);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // mainp
            // 
            this.mainp.AccessibleName = "mainp";
            this.mainp.BackColor = System.Drawing.Color.White;
            this.mainp.Controls.Add(this.wait);
            this.mainp.Controls.Add(this.pictureBox1);
            this.mainp.Location = new System.Drawing.Point(0, 0);
            this.mainp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mainp.Name = "mainp";
            this.mainp.Size = new System.Drawing.Size(456, 233);
            this.mainp.TabIndex = 2;
            // 
            // wait
            // 
            this.wait.AutoSize = true;
            this.wait.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wait.ForeColor = System.Drawing.Color.Gray;
            this.wait.Location = new System.Drawing.Point(133, 98);
            this.wait.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wait.Name = "wait";
            this.wait.Size = new System.Drawing.Size(186, 33);
            this.wait.TabIndex = 0;
            this.wait.Text = "Please Wait..";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Press ESC key to Exit...";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 267);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mainp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(50, 50);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keydown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.mainp.ResumeLayout(false);
            this.mainp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Panel mainp;
        public System.Windows.Forms.Label wait;
        private System.Windows.Forms.Label label1;


    }
}

