﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace kTsystem
{
    class invokets
    {
        static public void chstatus(string st)
        {
            try
            {
                Form1.agr1 inv = new Form1.agr1(invokets.chstatusi);
                staticstore.f1.Invoke(inv, st);
            }
            catch (Exception e)
            {
                log.store(staticstore.ticketname, "errorininvokets.cs1", e.Message);
                Environment.Exit(0);
            }
        }
        static public void chstatusi(string st)
        {
            try
            {
                staticstore.f1.wait.Text = st;
            }
            catch (Exception e)
            {
                log.store(staticstore.ticketname, "errorininvokets.cs2", e.Message);
            }
        }

        public static void closeme()
        {
            try
            {
                Form1.agr0 inc = new Form1.agr0(invokets.closemei);
                staticstore.f1.Invoke(inc);
            }
            catch (Exception e)
            {
                log.store(staticstore.ticketname, "errorininvokets.cs1", e.Message);
                Environment.Exit(0);
            }
        }
        public static void closemei()
        {
            try
            {
                bookwin bw = new bookwin();
                staticstore.f1.Hide();
                bw.ShowDialog();
                staticstore.f1.Close();
            }
            catch (Exception e)
            {
                log.store(staticstore.ticketname, "errorininvokets.cs5", e.Message);
            }
        }

        static public void invalidk()
        {
            try
            {
                Form1.agr0 inv = new Form1.agr0(invokets.invalidki);
                staticstore.f1.Invoke(inv);
            }
            catch (Exception e)
            {
                log.store(staticstore.ticketname, "errorininvokets.cs3", e.Message);
            }
        }
        static public void invalidki()
        {
            try
            {
                staticstore.f1.mainp.Controls.Clear();
                Label label1 = new Label();
                Label label2 = new Label();
                
                TextBox textBox1 = new TextBox();
                TextBox textBox2 = new TextBox();
                Button button1 = new Button();


                // 
                // label1
                // 
                label1.AutoSize = true;
                label1.Location = new System.Drawing.Point(32, 22);
                label1.Name = "label1";
                label1.Size = new System.Drawing.Size(66, 13);
                label1.TabIndex = 0;
                label1.Text = "Machine Id :";
                // 
                // label2
                // 
                label2.AutoSize = true;
                label2.Location = new System.Drawing.Point(32, 66);
                label2.Name = "label2";
                label2.Size = new System.Drawing.Size(71, 13);
                label2.TabIndex = 1;
                label2.Text = "Product Key :";
                // 
                
                // 
                // textBox1
                // 
                textBox1.Location = new System.Drawing.Point(108, 19);
                textBox1.Name = "textBox1";
                textBox1.Size = new System.Drawing.Size(188, 20);
                textBox1.TabIndex = 2;
                textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(staticstore.f1.keydown);
                // 
                // textBox2
                // 
                textBox2.Location = new System.Drawing.Point(108, 63);
                textBox2.Name = "textBox2";
                textBox2.Size = new System.Drawing.Size(188, 20);
                textBox2.TabIndex = 3;
                textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(staticstore.f1.keydown);
                // 
                // button1
                // 
                button1.Location = new System.Drawing.Point(221, 89);
                button1.Name = "button1";
                button1.Size = new System.Drawing.Size(75, 23);
                button1.TabIndex = 4;
                button1.Text = "Save";
                button1.UseVisualStyleBackColor = true;
                button1.Click += new System.EventHandler(staticstore.f1.Save_Click);
                button1.KeyDown += new System.Windows.Forms.KeyEventHandler(staticstore.f1.keydown);
                staticstore.f1.mainp.Controls.Add(label1);
                staticstore.f1.mainp.Controls.Add(label2);
                staticstore.f1.mainp.Controls.Add(textBox1);
                staticstore.f1.mainp.Controls.Add(textBox2);
                staticstore.f1.mainp.Controls.Add(button1);
                textBox1.Text = productkey.genrate();
            }
            catch (Exception e)
            {
                log.store(staticstore.ticketname, "errorininvokets.cs4", e.Message);
            }
        }
        
    }
}
