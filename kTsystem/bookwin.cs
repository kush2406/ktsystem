﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace kTsystem
{
    public partial class bookwin : Form
    {
        public SplitContainer splitContainer1;
        public Label statustxt;
        private edittick et;
        public CheckBox cp;
        public CheckBox at8;
        public CheckBox efb;
        public Button capbook;
        public TextBox captxt;
        public PictureBox pb;
        public Button refcap;
        public Label clscde;
        public Label sid;

        private DriveDetector driveDetector = null;

        public bookwin()
        {
            InitializeComponent();
            staticstore.bw = this;
            staticstore.addtick = false;
            driveDetector = new DriveDetector();
            driveDetector.DeviceRemoved += new DriveDetectorEventHandler(OnDriveRemoved);
        }

        private void OnDriveRemoved(object sender, DriveDetectorEventArgs e)
        {
            string drive = Path.GetDirectoryName(Application.ExecutablePath).Substring(0, 1);
            if (drive.ToUpper() == e.Drive.ToUpper().Substring(0, 1))
            {
                MessageBox.Show("Please Check Your Pendrive!");
                this.Close();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void removeProductKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr = MessageBox.Show("Do you really want to remove the Product Key?", "Key Remove", MessageBoxButtons.YesNo);
                if (dr.ToString() == "Yes")
                {
                    System.IO.File.Delete(@"pk.nfo");
                    this.Close();
                }
            }
            catch (Exception et)
            {
                log.store("default", "errorinbookwin.cs1", et.Message);
            }
        }

        private void editPaymentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addeditpay aep = new addeditpay();
            aep.Show();
        }

        private void getHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1. To create a New Ticket\n\tClick on File->New Ticket\n\tand fill Your Compleate Details of Ticket and click on Save\n\n2. To Edit Previous Tickets\n\tClick on File->Edit Tickets\n\tSelect Ticket to edit,Edit Ticket details and click on Save\n\n3. To Book Tickets\n\tClick on File->Add Tickets and Select Your Tickets\n\tOptions..\n\tAutomatic Book at 8::To start booking Ticket automatically at 8'o clock\n\tConfirm Payment::To alert Payment amount before payment\n\tAdd to booklist::To add ticket to the booking queue\n\nNOTE::You need to click on add to booklist also to book ticket automatic at 8", ":::Help Menu:::");
        }

        private void aboutProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string st = System.IO.File.ReadAllText(@"pk.nfo");
                int da = productkey.date(st);
                string date = (da % 100).ToString() + "/";
                da = da / 100;
                date = date + (da % 100).ToString() + "/";
                da = da / 100;
                date = date + (da).ToString();
                MessageBox.Show("Valid To : " + date, "About System");
            }
            catch (Exception f)
            {
                log.store("default", "Errorinnbookwin.cs2", f.Message);
            }
        }

        private void newTicketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newtick nt = new newtick();
            nt.ShowDialog();
        }

        private void editTicketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.Filter = "Ticket files (*.tkt)|*.tkt";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Title = "Select Ticket Files";
                et = new edittick();
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    staticstore.editf = openFileDialog1.FileName;
                    using (Stream myStream = openFileDialog1.OpenFile())
                    {
                        using (StreamReader wRead = new StreamReader(myStream))
                        {
                            while (!wRead.EndOfStream)
                            {
                                string st = wRead.ReadLine();
                                savetxt(st);
                            }
                        }
                    }
                }
                et.ShowDialog();
            }
            catch (Exception f)
            {
                log.store("default", "Errorinnbookwin.cs3", f.Message);
            }
        }
        private void savetxt(string st)
        {
            try
            {
                int index = st.IndexOf("=");
                string left = st.Substring(0, index);
                string right = st.Substring(index + 1);
                switch (left)
                {
                    case "stationFrom":
                        et.stationfrom.Text = right;
                        break;
                    case "stationTo":
                        et.stationto.Text = right;
                        break;
                    case "Jdate":
                        int ty = Convert.ToInt16(right.Substring(0, right.IndexOf(".")));
                        right = right.Substring(right.IndexOf(".") + 1);
                        int tm = Convert.ToInt16(right.Substring(0, right.IndexOf(".")));
                        right = right.Substring(right.IndexOf(".") + 1);
                        int td = Convert.ToInt16(right);
                        et.jdate.Value = new DateTime(ty, tm, td);//Convert.ToDateTime(right);
                        break;
                    case "clscode":
                        et.classcode.SelectedIndex = et.classcode.FindString(right);
                        break;
                    case "trainNo":
                        et.trainno.Text = right;
                        break;
                    case "boardPoint":
                        et.bdpt.Text = right;
                        break;
                    case "quota":
                        switch (right)
                        {
                            case "GN":
                                et.gn.Checked = true;
                                break;
                            case "CK":
                                et.ck.Checked = true;
                                break;
                            case "LD":
                                et.ld.Checked = true;
                                break;
                        }
                        break;
                    case "passengers[0].passengerName":
                        et.pn1.Text = right;
                        break;
                    case "passengers[0].passengerAge":
                        et.pa1.Text = right;
                        break;
                    case "passengers[0].passengerSex":
                        et.ps1.Text = right;
                        break;
                    case "passengers[0].berthPreffer":
                        et.pb1.Text = right;
                        break;
                    case "passengers[0].seniorCitizen":
                        et.psc1.Checked = true;
                        break;
                    case "passengers[1].passengerName":
                        et.pn2.Text = right;
                        break;
                    case "passengers[1].passengerAge":
                        et.pa2.Text = right;
                        break;
                    case "passengers[1].passengerSex":
                        et.ps2.Text = right;
                        break;
                    case "passengers[1].berthPreffer":
                        et.pb2.Text = right;
                        break;
                    case "passengers[1].seniorCitizen":
                        et.psc2.Checked = true;
                        break;
                    case "passengers[2].passengerName":
                        et.pn3.Text = right;
                        break;
                    case "passengers[2].passengerAge":
                        et.pa3.Text = right;
                        break;
                    case "passengers[2].passengerSex":
                        et.ps3.Text = right;
                        break;
                    case "passengers[2].berthPreffer":
                        et.pb3.Text = right;
                        break;
                    case "passengers[2].seniorCitizen":
                        et.psc3.Checked = true;
                        break;
                    case "passengers[3].passengerName":
                        et.pn4.Text = right;
                        break;
                    case "passengers[3].passengerAge":
                        et.pa4.Text = right;
                        break;
                    case "passengers[3].passengerSex":
                        et.ps4.Text = right;
                        break;
                    case "passengers[3].berthPreffer":
                        et.pb4.Text = right;
                        break;
                    case "passengers[3].seniorCitizen":
                        et.psc4.Checked = true;
                        break;
                    case "passengers[4].passengerName":
                        et.pn5.Text = right;
                        break;
                    case "passengers[4].passengerAge":
                        et.pa5.Text = right;
                        break;
                    case "passengers[4].passengerSex":
                        et.ps5.Text = right;
                        break;
                    case "passengers[4].berthPreffer":
                        et.pb5.Text = right;
                        break;
                    case "passengers[4].seniorCitizen":
                        et.psc5.Checked = true;
                        break;
                    case "passengers[5].passengerName":
                        et.pn6.Text = right;
                        break;
                    case "passengers[5].passengerAge":
                        et.pa6.Text = right;
                        break;
                    case "passengers[5].passengerSex":
                        et.ps6.Text = right;
                        break;
                    case "passengers[5].berthPreffer":
                        et.pb6.Text = right;
                        break;
                    case "passengers[5].seniorCitizen":
                        et.psc6.Checked = true;
                        break;
                    case "ChildPassengers[0].childPassengerName":
                        et.cpn1.Text = right;
                        break;
                    case "ChildPassengers[0].childPassengerAge":
                        et.cpa1.Text = right;
                        break;
                    case "ChildPassengers[0].childPassengerSex":
                        et.cps1.Text = right;
                        break;
                    case "ChildPassengers[1].childPassengerName":
                        et.cpn2.Text = right;
                        break;
                    case "ChildPassengers[1].childPassengerAge":
                        et.cpa2.Text = right;
                        break;
                    case "ChildPassengers[1].childPassengerSex":
                        et.cps2.Text = right;
                        break;
                    case "upgradeCh":
                        if (right == "on")
                            et.cfau.Checked = true;
                        break;
                    case "payment":
                        et.pay.SelectedIndex = et.pay.FindString(right);
                        break;
                    case "mobileNumber":
                        et.mno.Text = right;
                        break;
                    case "passengers[0].idCardType":
                        et.pictype1.SelectedIndex = System.Convert.ToInt32(right);
                        break;
                    case "passengers[0].idCardNo":
                        et.picard1.Text = right;
                        break;
                    case "passengers[1].idCardType":
                        et.pictype2.SelectedIndex = System.Convert.ToInt32(right);
                        break;
                    case "passengers[1].idCardNo":
                        et.picard2.Text = right;
                        break;
                    case "passengers[2].idCardType":
                        et.pictype3.SelectedIndex = System.Convert.ToInt32(right);
                        break;
                    case "passengers[2].idCardNo":
                        et.picard3.Text = right;
                        break;
                    case "passengers[3].idCardType":
                        et.pictype4.SelectedIndex = System.Convert.ToInt32(right);
                        break;
                    case "passengers[3].idCardNo":
                        et.picard4.Text = right;
                        break;
                    case "irctcu":
                        et.irctcu.Text = right;
                        break;
                    case "irctcp":
                        et.irctcp.Text = right;
                        break;
                    case "rstationFrom":
                        et.rstationfrom.Text = right;
                        break;
                    case "rstationTo":
                        et.rstationto.Text = right;
                        break;
                    case "rJdate":
                        int rty = Convert.ToInt16(right.Substring(0, right.IndexOf(".")));
                        right = right.Substring(right.IndexOf(".") + 1);
                        int rtm = Convert.ToInt16(right.Substring(0, right.IndexOf(".")));
                        right = right.Substring(right.IndexOf(".") + 1);
                        int rtd = Convert.ToInt16(right);
                        et.rjdate.Value = new DateTime(rty, rtm, rtd);//Convert.ToDateTime(right);
                        break;
                    case "rclscode":
                        et.rclasscode.SelectedIndex = et.rclasscode.FindString(right);
                        break;
                    case "rtrainNo":
                        et.rtrainno.Text = right;
                        break;
                    case "rboardPoint":
                        et.rbdpt.Text = right;
                        break;
                }
            }
            catch (Exception e)
            {
                log.store("default", "Errorinbookwin.cs4", e.Message);
            }
        }

        private void selectTicketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (staticstore.addtick == false)
            {
                addtick at = new addtick();
                at.start();
            }
            else
            {
                MessageBox.Show("You have already Selected a Ticket", "Error");
            }
        }

        private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.TopMost)
                {
                    this.TopMost = false;
                    this.alwaysOnTopToolStripMenuItem.Text = "Always on Top";
                }
                else
                {
                    this.TopMost = true;
                    this.alwaysOnTopToolStripMenuItem.Text = ">>Always on Top";
                }
            }
            catch (Exception et)
            {
                log.store("default", "errorinbookwin.cs6", et.Message);
            }
        }

        public void book_Click(object sender, EventArgs e)
        {
            /////check for error and not ready
            if (staticstore.status == "Not Ready" || staticstore.status == "error")
            {
                if (staticstore.status == "Not Ready")
                {
                    invokets.statustxt("Ready");
                    //////
                    int a = 0, b = 0;
                    if (staticstore.at8 == true) { a = 1; } else { a = 0; }
                    if (staticstore.efb == true) { b = 1; } else { b = 0; }
                    staticstore.booktype = a + b;
                    /////
                    staticstore.status = "ready";
                    staticstore.ustatus = "nready";
                    staticstore.cap = "";
                }
                else
                {
                    invokets.statustxt("Ready");
                    //////
                    int a = 0, b = 0;
                    if (staticstore.at8 == true) { a = 1; } else { a = 0; }
                    if (staticstore.efb == true) { b = 1; } else { b = 0; }
                    staticstore.booktype = a + b;
                    /////
                    staticstore.status = "worksucc";
                    staticstore.ustatus = "nready";
                }
            }
            else
            {
                MessageBox.Show("Booking In Progress..", "Error");
            }
        }

        public void efb_CheckedChanged(object sender, EventArgs e)
        {
            if (staticstore.status == "Not Ready" || staticstore.status == "error")
            {
                if (staticstore.bw.efb.Checked == true)
                {
                    if (staticstore.bw.at8.Checked == false)
                    {
                        MessageBox.Show("Atomatic Book at Best Time should be Checked Befor Enabling Fast Book", "Error");
                        staticstore.bw.efb.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        staticstore.efb = true;
                    }
                }
                else
                {
                    staticstore.efb = false;
                }
            }
            else
            {
                MessageBox.Show("Booking In Progress..", "Error");
            }
        }

        public void at8_CheckedChanged(object sender, EventArgs e)
        {
            if (staticstore.status == "Not Ready" || staticstore.status == "error")
            {
                if (staticstore.bw.at8.Checked == true)
                {
                    staticstore.at8 = true;
                }
                else
                {
                    staticstore.at8 = false;
                    staticstore.bw.efb.Checked = false;
                }
            }
            else
            {
                MessageBox.Show("Booking In Progress..", "Error");
            }
        }

        public void cp_CheckedChanged(object sender, EventArgs e)
        {
            if (staticstore.bw.cp.Checked == true)
            {
                staticstore.cp = true;
            }
            else
            {
                staticstore.cp = false;
            }
        }
        public void capbook_Click(object sender, EventArgs e)
        {
            staticstore.cap = captxt.Text;
            this.splitContainer1.Panel1.Controls.RemoveByKey("capbook");
            this.splitContainer1.Panel1.Controls.RemoveByKey("captxt");
            this.splitContainer1.Panel1.Controls.RemoveByKey("pb");
            this.splitContainer1.Panel1.Controls.RemoveByKey("refcap");
            splitContainer1.Height = 171;
            splitContainer1.SplitterDistance = 25;
            //staticstore.status = "selecttrnsucc";
        }

        public void refcap_Click(object sender, EventArgs e)
        {
            this.splitContainer1.Panel1.Controls.RemoveByKey("capbook");
            this.splitContainer1.Panel1.Controls.RemoveByKey("captxt");
            this.splitContainer1.Panel1.Controls.RemoveByKey("pb");
            this.splitContainer1.Panel1.Controls.RemoveByKey("refcap");
            timerss.getcap();
        }

        private void editPNRIssueTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editpnrtime ept = new editpnrtime();
            ept.ShowDialog();
        }

        private void addEditCardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            adcard adc = new adcard();
            adc.ShowDialog();
        }

        private void selectProxyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            proxyselect ps = new proxyselect();
            ps.ShowDialog();
        }

        private void goodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.goodToolStripMenuItem.Text = ">> Good";
            this.averageToolStripMenuItem.Text = "Average";
            this.worstToolStripMenuItem.Text = "Worst";
            staticstore.delay = 0;
        }

        private void averageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.goodToolStripMenuItem.Text = "Good";
            this.averageToolStripMenuItem.Text = ">> Average";
            this.worstToolStripMenuItem.Text = "Worst";
            staticstore.delay = 20000;
        }

        private void worstToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.goodToolStripMenuItem.Text = "Good";
            this.averageToolStripMenuItem.Text = "Average";
            this.worstToolStripMenuItem.Text = ">> Worst";
            staticstore.delay = 40000;
        }

        private void closeBrowserAfterPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeBrowserAfterPaymentToolStripMenuItem.Text = ">> Generate PNR in Software";
            this.geneatePNRInBrowserToolStripMenuItem.Text = "Generate PNR in Browser";
            staticstore.closebr = true;
        }

        private void geneatePNRInBrowserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.closeBrowserAfterPaymentToolStripMenuItem.Text = "Generate PNR in Software";
            this.geneatePNRInBrowserToolStripMenuItem.Text = ">> Generate PNR in Browser";
            staticstore.closebr = false;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("New Login Not Working Properly", "Error");
            //staticstore.selfetch = "N";
            //oldToolStripMenuItem.Text = "Old";
            //newToolStripMenuItem.Text = ">> New";
        }

        private void oldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            staticstore.selfetch = "O";
            oldToolStripMenuItem.Text = ">> Old";
            newToolStripMenuItem.Text = "New";
        }
    }
}
