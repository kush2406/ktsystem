﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace kTsystem
{
    public partial class edittick : Form
    {
        string trainName;
        string trainType;
        string departure;
        string arrival;
        string runsOn;
        string typ;
        string allClasses;
        string bvs;
        string bve;
        public edittick()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string gettxt()
        {
            try
            {
                string data = "stationFrom=" + stationfrom.Text +
                    "\nstationTo=" + stationto.Text +
                    "\nJdate=" + jdate.Value.Year.ToString() + "." + jdate.Value.Month.ToString() + "." + jdate.Value.Day.ToString() +
                    "\nclscode=" + classcode.Text +
                    "\ntrainNo=" + trainno.Text +
                    "\nboardPoint=" + bdpt.Text +
                    "\nquota=";
                if (gn.Checked)
                    data = data + "GN";
                if (ck.Checked)
                    data = data + "CK";
                if (ld.Checked)
                    data = data + "LD";
                data = data +
                    "\npassengers[0].passengerName=" + pn1.Text +
                    "\npassengers[0].passengerAge=" + pa1.Text +
                    "\npassengers[0].passengerSex=" + ps1.Text +
                    "\npassengers[0].berthPreffer=" + pb1.Text;
                if (psc1.Checked == true) { data = data + "\npassengers[0].seniorCitizen=SRCTZN"; }
                data = data +
                    "\npassengers[1].passengerName=" + pn2.Text +
                    "\npassengers[1].passengerAge=" + pa2.Text +
                    "\npassengers[1].passengerSex=" + ps2.Text +
                    "\npassengers[1].berthPreffer=" + pb2.Text;
                if (psc2.Checked == true) { data = data + "\npassengers[1].seniorCitizen=SRCTZN"; }
                data = data +
                    "\npassengers[2].passengerName=" + pn3.Text +
                    "\npassengers[2].passengerAge=" + pa3.Text +
                    "\npassengers[2].passengerSex=" + ps3.Text +
                    "\npassengers[2].berthPreffer=" + pb3.Text;
                if (psc3.Checked == true) { data = data + "\npassengers[2].seniorCitizen=SRCTZN"; }
                data = data +
                    "\npassengers[3].passengerName=" + pn4.Text +
                    "\npassengers[3].passengerAge=" + pa4.Text +
                    "\npassengers[3].passengerSex=" + ps4.Text +
                    "\npassengers[3].berthPreffer=" + pb4.Text;
                if (psc4.Checked == true) { data = data + "\npassengers[3].seniorCitizen=SRCTZN"; }
                data = data +
                    "\npassengers[4].passengerName=" + pn5.Text +
                    "\npassengers[4].passengerAge=" + pa5.Text +
                    "\npassengers[4].passengerSex=" + ps5.Text +
                    "\npassengers[4].berthPreffer=" + pb5.Text;
                if (psc5.Checked == true) { data = data + "\npassengers[4].seniorCitizen=SRCTZN"; }
                data = data +
                    "\npassengers[5].passengerName=" + pn6.Text +
                    "\npassengers[5].passengerAge=" + pa6.Text +
                    "\npassengers[5].passengerSex=" + ps6.Text +
                    "\npassengers[5].berthPreffer=" + pb6.Text;
                if (psc6.Checked == true) { data = data + "\npassengers[5].seniorCitizen=SRCTZN"; }
                data = data +
                    "\nChildPassengers[0].childPassengerName=" + cpn1.Text +
                    "\nChildPassengers[0].childPassengerAge=" + cpa1.Text +
                    "\nChildPassengers[0].childPassengerSex=" + cps1.Text +
                    "\nChildPassengers[1].childPassengerName=" + cpn2.Text +
                    "\nChildPassengers[1].childPassengerAge=" + cpa2.Text +
                    "\nChildPassengers[1].childPassengerSex=" + cps2.Text;
                if (cfau.Checked == true) { data = data + "\nupgradeCh=on"; }
                data = data +
                    "\npayment=" + pay.Text +
                    "\nmobileNumber=" + mno.Text +
                    "\npassengers[0].idCardType=" + pictype1.SelectedIndex.ToString() +
                    "\npassengers[0].idCardNo=" + picard1.Text +
                    "\npassengers[1].idCardType=" + pictype2.SelectedIndex.ToString() +
                    "\npassengers[1].idCardNo=" + picard2.Text +
                    "\npassengers[2].idCardType=" + pictype3.SelectedIndex.ToString() +
                    "\npassengers[2].idCardNo=" + picard3.Text +
                    "\npassengers[3].idCardType=" + pictype4.SelectedIndex.ToString() +
                    "\npassengers[3].idCardNo=" + picard4.Text +
                    "\nirctcu=" + irctcu.Text +
                    "\nirctcp=" + irctcp.Text +
                   "\nrstationFrom=" + rstationfrom.Text +
                    "\nrstationTo=" + rstationto.Text +
                    "\nrJdate=" + rjdate.Value.Year.ToString() + "." + rjdate.Value.Month.ToString() + "." + rjdate.Value.Day.ToString() +
                    "\nrclscode=" + rclasscode.Text +
                    "\nrtrainNo=" + rtrainno.Text +
                    "\nrboardPoint=" + rbdpt.Text;// +
                // "\ntrainname=" + this.trainName +
                // "\ntype=" + this.trainType +
                // "\ndeparture=" + this.departure +
                // "\narrival=" + this.arrival +
                // "\nrunson=" + this.runsOn +
                // "\ntyp=" + this.typ +
                // "\nallclasses=" + this.allClasses;
                return (data);
            }
            catch (Exception e)
            {
                log.store("default", "errorinedittick.cs1", e.Message);
                return "err";
            }
        }

        private bool login()
        {
            bool chk = false;
            status.Text = "Loggin into irctc";
            string url = "https://www.irctc.co.in/cgi-bin/bv60.dll/irctc/services/login.do";
            httpreq req = new httpreq();
            int time = 25000;
            string user = irctcu.Text;
            string pass = irctcp.Text;
            string post = "screen=home&userName=" + user + "&password=" + pass + "&button=Login";
            string reff = "";
            string result;
            do
            {
                result = req.request(url, post, time, reff, 0, 0);
                if (result != "retry")
                {
                    if (result != "error")
                    {
                        if (result.IndexOf("This Jsp displays the UserName/Password fields to login") < 0)
                        {
                            int bvs1 = result.IndexOf("BV_SessionID");
                            string bvs = result.Substring(bvs1 + 13);
                            int bvs2 = bvs.IndexOf("&");
                            this.bvs = bvs.Substring(0, bvs2);
                            int bve1 = result.IndexOf("BV_EngineID");
                            this.bve = result.Substring(bve1 + 12);
                            chk = fetch();
                        }
                        else
                        {
                            status.Text = "Invalid User Or Pass";
                        }
                    }
                }
            } while (result == "retry");
            return chk;
        }

        private bool fetch()
        {
            bool chk = false;
            status.Text = "Fetching Train List";
            string url = "https://www.irctc.co.in/cgi-bin/bv60.dll/irctc/booking/planner.do?BV_SessionID=" + bvs + "&BV_EngineID=" + bve;
            int time = 30000;
            string reff = "https://www.irctc.co.in/cgi-bin/bv60.dll/irctc/booking/planner.do?screen=fromlogin&BV_SessionID=" + bvs + "&BV_EngineID=" + bve;
            string result;
            httpreq req = new httpreq();
            do
            {
                string post = "BV_SessionID=" + bvs + "&BV_EngineID=" + bve + "&submitClicks=" + 0 + "&screen=trainsFromTo&browser=&pressedGo=&changetext=0&bookTicket=&stationFrom=" + stationfrom.Text + "&stationTo=" + stationto.Text + "&CurrentMonth=" + (System.Convert.ToInt32(DateTime.Now.Month) - 1) + "&CurrentDate=" + DateTime.Now.Day + "&CurrentYear=" + DateTime.Now.Year + "&day=" + jdate.Value.Day + "&month=" + jdate.Value.Month + "&year=" + jdate.Value.Year + "&JDatee1=" + jdate.Value.Day + "/" + jdate.Value.Month + "/" + jdate.Value.Year + "&userType=0&ticketType=eticket&quota=GN&timedate=" + DateTime.Now.Hour + "&Submit=Find Trains&selectedIndex=1&userName=&password=&backRoute=true";
                result = req.request(url, post, time, reff, 0, 15000);
                result = staticm.errochk(result, "newtick");
                if (result == "retry")
                {
                    log.store("Newticket", "retryinfetchtrain::id-" + 0, "0");
                }
                else
                {
                    if (result != "error")
                    {
                        string clcode = classcode.Text;
                        clcode = clcode.Substring(clcode.IndexOf("(") + 1);
                        clcode = clcode.Substring(0, clcode.IndexOf(")"));
                        int ind = result.IndexOf("setvalue('" + clcode + "','" + trainno.Text + "',");
                        if (ind == -1)
                        {
                            ind = result.IndexOf("setvalue('" + clcode + "',\r\n                                        '" + trainno.Text + "',");
                        }
                        string data = "";
                        if (ind > -1)
                        {
                            data = result.Substring(ind);
                            data = data.Substring(data.IndexOf("(") + 1);
                            data = data.Substring(0, data.IndexOf(")"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            this.trainName = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            this.trainType = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            this.departure = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            this.arrival = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            this.runsOn = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            if (stationfrom.Text != data.Substring(0, data.IndexOf("'")))
                            {
                                if (MessageBox.Show("Trains From Station of Ticket does not match with the code you have entered\nChange Station Code to " + data.Substring(0, data.IndexOf("'")), "Verify Ticket Details", MessageBoxButtons.YesNo).ToString() == "Yes")
                                {
                                    stationfrom.Text = data.Substring(0, data.IndexOf("'"));
                                }
                                if (MessageBox.Show("Change Boarding Point to : " + stationfrom.Text, "Verify Ticket Details", MessageBoxButtons.YesNo).ToString() == "Yes")
                                {
                                    bdpt.Text = stationfrom.Text;
                                }
                            }
                            //staticstore.runsOn = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            if (stationto.Text != data.Substring(0, data.IndexOf("'")))
                            {
                                if (MessageBox.Show("Trains To Station of Ticket does not match with the code you have entered\nChange Station Code to " + data.Substring(0, data.IndexOf("'")), "Verify Ticket Details", MessageBoxButtons.YesNo).ToString() == "Yes")
                                {
                                    stationto.Text = data.Substring(0, data.IndexOf("'"));
                                }
                            }
                            //staticstore.runsOn = data.Substring(0, data.IndexOf("'"));
                            data = data.Substring(data.IndexOf(",") + 1);
                            data = data.Substring(data.IndexOf("'") + 1);
                            this.typ = data.Substring(0, data.IndexOf("'"));
                            this.allClasses = "";
                            if (typ.IndexOf("2S") != -1 && staticstore.trainType == "S")
                            {
                                this.allClasses = "JS";
                            }
                            chk = true;
                        }
                        else
                        {
                            MessageBox.Show("Invalid Ticket Details Please Check", "New Ticket");
                        }
                    }
                }
            } while (result == "retry");
            return chk;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //button1.Visible = false;
            //if (login() == true)
            {
                //    if (MessageBox.Show("Is Imformation Correct ?\nTrain Name : " + this.trainName + "\nDeparture : " + this.departure + "\nArrival : " + this.arrival + "\nRuns On : " + runsOn, "New Ticket", MessageBoxButtons.YesNo).ToString() == "Yes")
                {
                    try
                    {
                        System.IO.File.WriteAllText(staticstore.editf, gettxt());
                        this.Close();
                    }
                    catch (Exception ef)
                    {
                        MessageBox.Show("There was Problem in saving yours File", "Error");
                        log.store("default", "Errorinedittick1", ef.Message);
                    }
                }
                //else
                //{
                //    button1.Visible = true;
                //}
            }
            //else
            //{
            //    button1.Visible = true;
            //}
        }

        private void focusfrom(object sender, EventArgs e)
        {
            stationfrom.Text = stationfrom.Text.ToUpper();
            bdpt.Text = stationfrom.Text;
        }

        private void focusto(object sender, EventArgs e)
        {
            stationto.Text = stationto.Text.ToUpper();
        }

        private void classcode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = classcode.SelectedIndex;
                switch (index)
                {
                    case 0:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "No Choice" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "No Choice" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "No Choice" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "No Choice" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "No Choice" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "No Choice" });
                        break;
                    case 1:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Lower", "Upper" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Lower", "Upper" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Lower", "Upper" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Lower", "Upper" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Lower", "Upper" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Lower", "Upper" });
                        break;
                    case 2:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Lower", "Upper", "Side lower", "Side upper" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Lower", "Upper", "Side lower", "Side upper" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Lower", "Upper", "Side lower", "Side upper" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Lower", "Upper", "Side lower", "Side upper" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Lower", "Upper", "Side lower", "Side upper" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Lower", "Upper", "Side lower", "Side upper" });
                        break;
                    case 3:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        break;
                    case 7:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper", "Side middle" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper", "Side middle" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper", "Side middle" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper", "Side middle" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper", "Side middle" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper", "Side middle" });
                        break;
                    case 4:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        break;
                    case 5:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Lower", "Upper", "Middle", "Side lower", "Side upper" });
                        break;
                    case 6:
                        pb1.Items.Clear();
                        pb1.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb2.Items.Clear();
                        pb2.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb3.Items.Clear();
                        pb3.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb4.Items.Clear();
                        pb4.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb5.Items.Clear();
                        pb5.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        pb6.Items.Clear();
                        pb6.Items.AddRange(new object[] { "Window Seat", "No Choice" });
                        break;
                }
            }
            catch (Exception et)
            {
                log.store("default", "errorinnewtick.cs3", et.Message);
            }
        }

        private void rfocusfrom(object sender, EventArgs e)
        {
            rstationfrom.Text = rstationfrom.Text.ToUpper();
            rbdpt.Text = rstationfrom.Text;
        }

        private void efocusto(object sender, EventArgs e)
        {
            rstationto.Text = rstationto.Text.ToUpper();
        }
    }
}
