﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kTsystem
{
    static class crypto
    {
        public static UInt64 EncryptString(UInt64 me)
        {
            try
            {
                Random r = new Random();
                int no = r.Next(9);
                UInt64 ret = 0;
                while (me != 0)
                {
                    UInt64 te = me % 10;
                    UInt64 te1 = te + (UInt64)no;
                    if (te1 >= 10)
                    {
                        te1 = te1 % 10;
                    }
                    ret = ret * 10 + (UInt64)te1;
                    me = me / 10;
                }
                ret = ret * 10 + (UInt64)no;
                return ret;
            }
            catch (Exception e)
            {
                log.store("default", "Errorincrypto1.cs", e.Message);
                return 0;
            }
        }

        public static UInt64 DecryptString(UInt64 me)
        {
            try
            {
                UInt64 no = me % 10;
                me = me / 10;
                UInt64 ret = 0;
                while (me != 0)
                {
                    UInt64 te = me % 10;
                    if (te < no)
                    {
                        te = te + 10;
                    }
                    UInt64 te1 = te - no;
                    ret = ret * 10 + (UInt64)te1;
                    me = me / 10;
                }
                return ret;
            }
            catch (Exception e)
            {
                log.store("default", "Errorincrypto2.cs", e.Message);
                return 0;
            }
        }
    }
}