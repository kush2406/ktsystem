﻿namespace kTsystem
{
    partial class edittick
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.pictype1 = new System.Windows.Forms.ComboBox();
            this.picard1 = new System.Windows.Forms.TextBox();
            this.pictype2 = new System.Windows.Forms.ComboBox();
            this.pictype4 = new System.Windows.Forms.ComboBox();
            this.pictype3 = new System.Windows.Forms.ComboBox();
            this.picard2 = new System.Windows.Forms.TextBox();
            this.picard3 = new System.Windows.Forms.TextBox();
            this.picard4 = new System.Windows.Forms.TextBox();
            this.status = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.rstationfrom = new System.Windows.Forms.TextBox();
            this.rtrainno = new System.Windows.Forms.TextBox();
            this.rstationto = new System.Windows.Forms.TextBox();
            this.rbdpt = new System.Windows.Forms.TextBox();
            this.rclasscode = new System.Windows.Forms.ComboBox();
            this.rjdate = new System.Windows.Forms.DateTimePicker();
            this.paymenttype = new System.Windows.Forms.Label();
            this.pay = new System.Windows.Forms.ComboBox();
            this.irctcp = new System.Windows.Forms.TextBox();
            this.irctcu = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.mno = new System.Windows.Forms.TextBox();
            this.cfau = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cpn1 = new System.Windows.Forms.TextBox();
            this.cpn2 = new System.Windows.Forms.TextBox();
            this.cpa1 = new System.Windows.Forms.ComboBox();
            this.cpa2 = new System.Windows.Forms.ComboBox();
            this.cps2 = new System.Windows.Forms.ComboBox();
            this.cps1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pn1 = new System.Windows.Forms.TextBox();
            this.pn3 = new System.Windows.Forms.TextBox();
            this.pn2 = new System.Windows.Forms.TextBox();
            this.pn4 = new System.Windows.Forms.TextBox();
            this.pn5 = new System.Windows.Forms.TextBox();
            this.pn6 = new System.Windows.Forms.TextBox();
            this.pa1 = new System.Windows.Forms.TextBox();
            this.pa2 = new System.Windows.Forms.TextBox();
            this.pa3 = new System.Windows.Forms.TextBox();
            this.pa4 = new System.Windows.Forms.TextBox();
            this.pa5 = new System.Windows.Forms.TextBox();
            this.pa6 = new System.Windows.Forms.TextBox();
            this.ps1 = new System.Windows.Forms.ComboBox();
            this.ps2 = new System.Windows.Forms.ComboBox();
            this.ps3 = new System.Windows.Forms.ComboBox();
            this.ps4 = new System.Windows.Forms.ComboBox();
            this.ps5 = new System.Windows.Forms.ComboBox();
            this.ps6 = new System.Windows.Forms.ComboBox();
            this.pb1 = new System.Windows.Forms.ComboBox();
            this.pb2 = new System.Windows.Forms.ComboBox();
            this.pb3 = new System.Windows.Forms.ComboBox();
            this.pb4 = new System.Windows.Forms.ComboBox();
            this.pb5 = new System.Windows.Forms.ComboBox();
            this.pb6 = new System.Windows.Forms.ComboBox();
            this.psc1 = new System.Windows.Forms.CheckBox();
            this.psc2 = new System.Windows.Forms.CheckBox();
            this.psc3 = new System.Windows.Forms.CheckBox();
            this.psc4 = new System.Windows.Forms.CheckBox();
            this.psc5 = new System.Windows.Forms.CheckBox();
            this.psc6 = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.stationfrom = new System.Windows.Forms.TextBox();
            this.trainno = new System.Windows.Forms.TextBox();
            this.stationto = new System.Windows.Forms.TextBox();
            this.bdpt = new System.Windows.Forms.TextBox();
            this.classcode = new System.Windows.Forms.ComboBox();
            this.gn = new System.Windows.Forms.RadioButton();
            this.ck = new System.Windows.Forms.RadioButton();
            this.ld = new System.Windows.Forms.RadioButton();
            this.jdate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.tableLayoutPanel5);
            this.panel1.Controls.Add(this.status);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.paymenttype);
            this.panel1.Controls.Add(this.pay);
            this.panel1.Controls.Add(this.irctcp);
            this.panel1.Controls.Add(this.irctcu);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.mno);
            this.panel1.Controls.Add(this.cfau);
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(665, 569);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel5.Controls.Add(this.label33, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label34, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label35, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label36, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label32, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label37, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.pictype1, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.picard1, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.pictype2, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.pictype4, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.pictype3, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.picard2, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.picard3, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.picard4, 2, 4);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(11, 708);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(623, 151);
            this.tableLayoutPanel5.TabIndex = 27;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 17);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(16, 17);
            this.label33.TabIndex = 1;
            this.label33.Text = "1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 49);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(16, 17);
            this.label34.TabIndex = 2;
            this.label34.Text = "2";
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(4, 81);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(47, 25);
            this.label35.TabIndex = 0;
            this.label35.Text = "3";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(4, 113);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 25);
            this.label36.TabIndex = 0;
            this.label36.Text = "4";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(66, 0);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(103, 17);
            this.label32.TabIndex = 0;
            this.label32.Text = "ID Card Type";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(346, 0);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 17);
            this.label37.TabIndex = 0;
            this.label37.Text = "ID Card No";
            // 
            // pictype1
            // 
            this.pictype1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pictype1.FormattingEnabled = true;
            this.pictype1.Items.AddRange(new object[] {
            "Driving License",
            "Passport",
            "Pan Card",
            "Voter Identity Card",
            "Central/State Government issued ID Card",
            "Student Identity Card",
            "Nationalised Bank Passbook",
            "Credit Cards issued by Banks",
            "Unique I-Card"});
            this.pictype1.Location = new System.Drawing.Point(66, 21);
            this.pictype1.Margin = new System.Windows.Forms.Padding(4);
            this.pictype1.Name = "pictype1";
            this.pictype1.Size = new System.Drawing.Size(220, 24);
            this.pictype1.TabIndex = 3;
            // 
            // picard1
            // 
            this.picard1.Location = new System.Drawing.Point(346, 21);
            this.picard1.Margin = new System.Windows.Forms.Padding(4);
            this.picard1.Name = "picard1";
            this.picard1.Size = new System.Drawing.Size(209, 22);
            this.picard1.TabIndex = 4;
            // 
            // pictype2
            // 
            this.pictype2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pictype2.FormattingEnabled = true;
            this.pictype2.Items.AddRange(new object[] {
            "Driving License",
            "Passport",
            "Pan Card",
            "Voter Identity Card",
            "Central/State Government issued ID Card",
            "Student Identity Card",
            "Nationalised Bank Passbook",
            "Credit Cards issued by Banks",
            "Unique I-Card"});
            this.pictype2.Location = new System.Drawing.Point(66, 53);
            this.pictype2.Margin = new System.Windows.Forms.Padding(4);
            this.pictype2.Name = "pictype2";
            this.pictype2.Size = new System.Drawing.Size(220, 24);
            this.pictype2.TabIndex = 3;
            // 
            // pictype4
            // 
            this.pictype4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pictype4.FormattingEnabled = true;
            this.pictype4.Items.AddRange(new object[] {
            "Driving License",
            "Passport",
            "Pan Card",
            "Voter Identity Card",
            "Central/State Government issued ID Card",
            "Student Identity Card",
            "Nationalised Bank Passbook",
            "Credit Cards issued by Banks",
            "Unique I-Card"});
            this.pictype4.Location = new System.Drawing.Point(66, 117);
            this.pictype4.Margin = new System.Windows.Forms.Padding(4);
            this.pictype4.Name = "pictype4";
            this.pictype4.Size = new System.Drawing.Size(220, 24);
            this.pictype4.TabIndex = 3;
            // 
            // pictype3
            // 
            this.pictype3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pictype3.FormattingEnabled = true;
            this.pictype3.Items.AddRange(new object[] {
            "Driving License",
            "Passport",
            "Pan Card",
            "Voter Identity Card",
            "Central/State Government issued ID Card",
            "Student Identity Card",
            "Nationalised Bank Passbook",
            "Credit Cards issued by Banks",
            "Unique I-Card"});
            this.pictype3.Location = new System.Drawing.Point(66, 85);
            this.pictype3.Margin = new System.Windows.Forms.Padding(4);
            this.pictype3.Name = "pictype3";
            this.pictype3.Size = new System.Drawing.Size(220, 24);
            this.pictype3.TabIndex = 3;
            // 
            // picard2
            // 
            this.picard2.Location = new System.Drawing.Point(346, 53);
            this.picard2.Margin = new System.Windows.Forms.Padding(4);
            this.picard2.Name = "picard2";
            this.picard2.Size = new System.Drawing.Size(209, 22);
            this.picard2.TabIndex = 4;
            // 
            // picard3
            // 
            this.picard3.Location = new System.Drawing.Point(346, 85);
            this.picard3.Margin = new System.Windows.Forms.Padding(4);
            this.picard3.Name = "picard3";
            this.picard3.Size = new System.Drawing.Size(209, 22);
            this.picard3.TabIndex = 4;
            // 
            // picard4
            // 
            this.picard4.Location = new System.Drawing.Point(346, 117);
            this.picard4.Margin = new System.Windows.Forms.Padding(4);
            this.picard4.Name = "picard4";
            this.picard4.Size = new System.Drawing.Size(209, 22);
            this.picard4.TabIndex = 4;
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.Location = new System.Drawing.Point(17, 1056);
            this.status.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(188, 17);
            this.status.TabIndex = 26;
            this.status.Text = "Compleate Ticket Details";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel4);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(16, 916);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(603, 138);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Get Fare Details";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.90558F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.09442F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 294F));
            this.tableLayoutPanel4.Controls.Add(this.label39, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label40, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label41, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label43, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label44, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.label45, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.rstationfrom, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.rtrainno, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.rstationto, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.rbdpt, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.rclasscode, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.rjdate, 1, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 23);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(621, 127);
            this.tableLayoutPanel4.TabIndex = 17;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(4, 6);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(48, 17);
            this.label39.TabIndex = 0;
            this.label39.Text = "From :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(4, 37);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(46, 17);
            this.label40.TabIndex = 0;
            this.label40.Text = "Date :";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(4, 70);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(45, 34);
            this.label41.TabIndex = 0;
            this.label41.Text = "Train No :";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(205, 6);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(33, 17);
            this.label43.TabIndex = 0;
            this.label43.Text = "To :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(205, 70);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 17);
            this.label44.TabIndex = 0;
            this.label44.Text = "Bdg Pt :";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(205, 37);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(50, 17);
            this.label45.TabIndex = 0;
            this.label45.Text = "Class :";
            // 
            // rstationfrom
            // 
            this.rstationfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rstationfrom.Location = new System.Drawing.Point(72, 4);
            this.rstationfrom.Margin = new System.Windows.Forms.Padding(4);
            this.rstationfrom.Name = "rstationfrom";
            this.rstationfrom.Size = new System.Drawing.Size(125, 23);
            this.rstationfrom.TabIndex = 1;
            this.rstationfrom.Leave += new System.EventHandler(this.rfocusfrom);
            // 
            // rtrainno
            // 
            this.rtrainno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtrainno.Location = new System.Drawing.Point(72, 68);
            this.rtrainno.Margin = new System.Windows.Forms.Padding(4);
            this.rtrainno.Name = "rtrainno";
            this.rtrainno.Size = new System.Drawing.Size(125, 23);
            this.rtrainno.TabIndex = 5;
            // 
            // rstationto
            // 
            this.rstationto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rstationto.Location = new System.Drawing.Point(330, 4);
            this.rstationto.Margin = new System.Windows.Forms.Padding(4);
            this.rstationto.Name = "rstationto";
            this.rstationto.Size = new System.Drawing.Size(132, 23);
            this.rstationto.TabIndex = 2;
            this.rstationto.Leave += new System.EventHandler(this.efocusto);
            // 
            // rbdpt
            // 
            this.rbdpt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbdpt.Location = new System.Drawing.Point(330, 68);
            this.rbdpt.Margin = new System.Windows.Forms.Padding(4);
            this.rbdpt.Name = "rbdpt";
            this.rbdpt.Size = new System.Drawing.Size(132, 23);
            this.rbdpt.TabIndex = 6;
            // 
            // rclasscode
            // 
            this.rclasscode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rclasscode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rclasscode.FormattingEnabled = true;
            this.rclasscode.Items.AddRange(new object[] {
            "First Class AC(1A)",
            "First Class(FC)",
            "AC 2-tier sleeper(2A)",
            "AC 3 Tier(3A)",
            "AC chair Car(CC)",
            "Sleeper Class(SL)",
            "Second Sitting(2S)",
            "AC 3 Tier Economy(3E)"});
            this.rclasscode.Location = new System.Drawing.Point(330, 35);
            this.rclasscode.Margin = new System.Windows.Forms.Padding(4);
            this.rclasscode.Name = "rclasscode";
            this.rclasscode.Size = new System.Drawing.Size(160, 25);
            this.rclasscode.TabIndex = 4;
            // 
            // rjdate
            // 
            this.rjdate.CustomFormat = "dd/MM/yyyy";
            this.rjdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rjdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.rjdate.Location = new System.Drawing.Point(72, 35);
            this.rjdate.Margin = new System.Windows.Forms.Padding(4);
            this.rjdate.Name = "rjdate";
            this.rjdate.Size = new System.Drawing.Size(125, 23);
            this.rjdate.TabIndex = 3;
            // 
            // paymenttype
            // 
            this.paymenttype.AutoSize = true;
            this.paymenttype.Location = new System.Drawing.Point(24, 636);
            this.paymenttype.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.paymenttype.Name = "paymenttype";
            this.paymenttype.Size = new System.Drawing.Size(114, 17);
            this.paymenttype.TabIndex = 0;
            this.paymenttype.Text = "Select Payment :";
            // 
            // pay
            // 
            this.pay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pay.FormattingEnabled = true;
            this.pay.Items.AddRange(new object[] {
            "#######CREDIT CARDS#######",
            "Visa/Master (Powered by ICICI Bank)",
            "Visa/Master (Powered by HDFC Bank)",
            "Visa/Master (Powered by CITI Bank)",
            "Visa/Master (Powered by AXIS Bank)",
            "American Express",
            "#######NET BANKING#######",
            "ICICI Bank",
            "HDFC Bank",
            "AXIS Bank",
            "IDBI Bank",
            "SBI - Internet Banking",
            "Punjab National Bank",
            "Syndicate Bank",
            "Oriental Bank Of Commerce",
            "Corporation Bank",
            "Bank Of India",
            "Indian Bank",
            "SBI Associate Bank\'s",
            "Union Bank Of India",
            "Bank of Baroda",
            "#######DEBIT CARDS#######",
            "CITI Bank",
            "SBI ATM-cum-Debit Card",
            "Punjab_National_Bank",
            "Andhra  Bank",
            "Indian_Bank",
            "Canara Bank",
            "#######CASH CARDS#######",
            "ITZ Cash Card",
            "I Cash Card",
            "Oxi Cash",
            "#######EMI OPTIONS#######",
            "ICICI Bank EMI",
            "CITI Bank EMI"});
            this.pay.Location = new System.Drawing.Point(160, 633);
            this.pay.Margin = new System.Windows.Forms.Padding(4);
            this.pay.Name = "pay";
            this.pay.Size = new System.Drawing.Size(275, 24);
            this.pay.TabIndex = 4;
            // 
            // irctcp
            // 
            this.irctcp.Location = new System.Drawing.Point(451, 875);
            this.irctcp.Margin = new System.Windows.Forms.Padding(4);
            this.irctcp.Name = "irctcp";
            this.irctcp.Size = new System.Drawing.Size(132, 22);
            this.irctcp.TabIndex = 11;
            // 
            // irctcu
            // 
            this.irctcu.Location = new System.Drawing.Point(160, 875);
            this.irctcu.Margin = new System.Windows.Forms.Padding(4);
            this.irctcu.Name = "irctcu";
            this.irctcu.Size = new System.Drawing.Size(132, 22);
            this.irctcu.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(325, 879);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(113, 17);
            this.label31.TabIndex = 0;
            this.label31.Text = "IRCTC Password";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(24, 879);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(125, 17);
            this.label30.TabIndex = 0;
            this.label30.Text = "IRCTC Username :";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(529, 1056);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 14;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(388, 1056);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 13;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // mno
            // 
            this.mno.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.mno.Location = new System.Drawing.Point(216, 670);
            this.mno.Margin = new System.Windows.Forms.Padding(4);
            this.mno.MaxLength = 10;
            this.mno.Name = "mno";
            this.mno.Size = new System.Drawing.Size(132, 22);
            this.mno.TabIndex = 5;
            // 
            // cfau
            // 
            this.cfau.AutoSize = true;
            this.cfau.Location = new System.Drawing.Point(24, 601);
            this.cfau.Margin = new System.Windows.Forms.Padding(4);
            this.cfau.Name = "cfau";
            this.cfau.Size = new System.Drawing.Size(222, 21);
            this.cfau.TabIndex = 3;
            this.cfau.Text = "Consider for Auto Upgradation";
            this.cfau.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 216F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 216F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel3.Controls.Add(this.label24, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label25, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label26, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label27, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label28, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label29, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.cpn1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.cpn2, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.cpa1, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.cpa2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.cps2, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.cps1, 3, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(16, 486);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(621, 89);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(4, 0);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 20);
            this.label24.TabIndex = 0;
            this.label24.Text = "SNo";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(79, 0);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 20);
            this.label25.TabIndex = 0;
            this.label25.Text = "Name";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(295, 0);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 20);
            this.label26.TabIndex = 0;
            this.label26.Text = "Age";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(511, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 20);
            this.label27.TabIndex = 0;
            this.label27.Text = "Sex";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(4, 20);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(16, 17);
            this.label28.TabIndex = 0;
            this.label28.Text = "1";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(4, 52);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(16, 17);
            this.label29.TabIndex = 0;
            this.label29.Text = "2";
            // 
            // cpn1
            // 
            this.cpn1.Location = new System.Drawing.Point(79, 24);
            this.cpn1.Margin = new System.Windows.Forms.Padding(4);
            this.cpn1.MaxLength = 16;
            this.cpn1.Name = "cpn1";
            this.cpn1.Size = new System.Drawing.Size(132, 22);
            this.cpn1.TabIndex = 0;
            // 
            // cpn2
            // 
            this.cpn2.Location = new System.Drawing.Point(79, 56);
            this.cpn2.Margin = new System.Windows.Forms.Padding(4);
            this.cpn2.MaxLength = 16;
            this.cpn2.Name = "cpn2";
            this.cpn2.Size = new System.Drawing.Size(132, 22);
            this.cpn2.TabIndex = 3;
            // 
            // cpa1
            // 
            this.cpa1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cpa1.FormattingEnabled = true;
            this.cpa1.Items.AddRange(new object[] {
            "Below one year",
            "One year",
            "Two year",
            "Three year",
            "Four year"});
            this.cpa1.Location = new System.Drawing.Point(295, 24);
            this.cpa1.Margin = new System.Windows.Forms.Padding(4);
            this.cpa1.Name = "cpa1";
            this.cpa1.Size = new System.Drawing.Size(160, 24);
            this.cpa1.TabIndex = 1;
            // 
            // cpa2
            // 
            this.cpa2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cpa2.FormattingEnabled = true;
            this.cpa2.Items.AddRange(new object[] {
            "Below one year",
            "One year",
            "Two year",
            "Three year",
            "Four year"});
            this.cpa2.Location = new System.Drawing.Point(295, 56);
            this.cpa2.Margin = new System.Windows.Forms.Padding(4);
            this.cpa2.Name = "cpa2";
            this.cpa2.Size = new System.Drawing.Size(160, 24);
            this.cpa2.TabIndex = 4;
            // 
            // cps2
            // 
            this.cps2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cps2.FormattingEnabled = true;
            this.cps2.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cps2.Location = new System.Drawing.Point(511, 56);
            this.cps2.Margin = new System.Windows.Forms.Padding(4);
            this.cps2.Name = "cps2";
            this.cps2.Size = new System.Drawing.Size(105, 24);
            this.cps2.TabIndex = 5;
            // 
            // cps1
            // 
            this.cps1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cps1.FormattingEnabled = true;
            this.cps1.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cps1.Location = new System.Drawing.Point(511, 24);
            this.cps1.Margin = new System.Windows.Forms.Padding(4);
            this.cps1.Name = "cps1";
            this.cps1.Size = new System.Drawing.Size(105, 24);
            this.cps1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label13, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label14, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label16, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label17, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label20, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label22, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label23, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.pn1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.pn3, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.pn2, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.pn4, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.pn5, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.pn6, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.pa1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.pa2, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.pa3, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.pa4, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.pa5, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.pa6, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.ps1, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.ps2, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.ps3, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.ps4, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.ps5, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.ps6, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.pb1, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.pb2, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.pb3, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.pb4, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.pb5, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.pb6, 4, 6);
            this.tableLayoutPanel2.Controls.Add(this.psc1, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.psc2, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.psc3, 5, 3);
            this.tableLayoutPanel2.Controls.Add(this.psc4, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this.psc5, 5, 5);
            this.tableLayoutPanel2.Controls.Add(this.psc6, 5, 6);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(16, 222);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(621, 219);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(4, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "SNo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(51, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(210, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Age";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(259, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Sex";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(362, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Birth Prefrence";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(519, 0);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Senior Citizen";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 20);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 17);
            this.label18.TabIndex = 0;
            this.label18.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 52);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 84);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 17);
            this.label20.TabIndex = 0;
            this.label20.Text = "3";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 116);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "4";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 148);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 17);
            this.label22.TabIndex = 0;
            this.label22.Text = "5";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(4, 180);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 17);
            this.label23.TabIndex = 0;
            this.label23.Text = "6";
            // 
            // pn1
            // 
            this.pn1.Location = new System.Drawing.Point(51, 24);
            this.pn1.Margin = new System.Windows.Forms.Padding(4);
            this.pn1.MaxLength = 16;
            this.pn1.Name = "pn1";
            this.pn1.Size = new System.Drawing.Size(132, 22);
            this.pn1.TabIndex = 0;
            // 
            // pn3
            // 
            this.pn3.Location = new System.Drawing.Point(51, 88);
            this.pn3.Margin = new System.Windows.Forms.Padding(4);
            this.pn3.MaxLength = 16;
            this.pn3.Name = "pn3";
            this.pn3.Size = new System.Drawing.Size(132, 22);
            this.pn3.TabIndex = 10;
            // 
            // pn2
            // 
            this.pn2.Location = new System.Drawing.Point(51, 56);
            this.pn2.Margin = new System.Windows.Forms.Padding(4);
            this.pn2.MaxLength = 16;
            this.pn2.Name = "pn2";
            this.pn2.Size = new System.Drawing.Size(132, 22);
            this.pn2.TabIndex = 5;
            // 
            // pn4
            // 
            this.pn4.Location = new System.Drawing.Point(51, 120);
            this.pn4.Margin = new System.Windows.Forms.Padding(4);
            this.pn4.MaxLength = 16;
            this.pn4.Name = "pn4";
            this.pn4.Size = new System.Drawing.Size(132, 22);
            this.pn4.TabIndex = 15;
            // 
            // pn5
            // 
            this.pn5.Location = new System.Drawing.Point(51, 152);
            this.pn5.Margin = new System.Windows.Forms.Padding(4);
            this.pn5.MaxLength = 16;
            this.pn5.Name = "pn5";
            this.pn5.Size = new System.Drawing.Size(132, 22);
            this.pn5.TabIndex = 20;
            // 
            // pn6
            // 
            this.pn6.Location = new System.Drawing.Point(51, 184);
            this.pn6.Margin = new System.Windows.Forms.Padding(4);
            this.pn6.MaxLength = 16;
            this.pn6.Name = "pn6";
            this.pn6.Size = new System.Drawing.Size(132, 22);
            this.pn6.TabIndex = 25;
            // 
            // pa1
            // 
            this.pa1.Location = new System.Drawing.Point(210, 24);
            this.pa1.Margin = new System.Windows.Forms.Padding(4);
            this.pa1.MaxLength = 3;
            this.pa1.Name = "pa1";
            this.pa1.Size = new System.Drawing.Size(40, 22);
            this.pa1.TabIndex = 1;
            // 
            // pa2
            // 
            this.pa2.Location = new System.Drawing.Point(210, 56);
            this.pa2.Margin = new System.Windows.Forms.Padding(4);
            this.pa2.MaxLength = 3;
            this.pa2.Name = "pa2";
            this.pa2.Size = new System.Drawing.Size(40, 22);
            this.pa2.TabIndex = 6;
            // 
            // pa3
            // 
            this.pa3.Location = new System.Drawing.Point(210, 88);
            this.pa3.Margin = new System.Windows.Forms.Padding(4);
            this.pa3.MaxLength = 3;
            this.pa3.Name = "pa3";
            this.pa3.Size = new System.Drawing.Size(40, 22);
            this.pa3.TabIndex = 11;
            // 
            // pa4
            // 
            this.pa4.Location = new System.Drawing.Point(210, 120);
            this.pa4.Margin = new System.Windows.Forms.Padding(4);
            this.pa4.MaxLength = 3;
            this.pa4.Name = "pa4";
            this.pa4.Size = new System.Drawing.Size(40, 22);
            this.pa4.TabIndex = 16;
            // 
            // pa5
            // 
            this.pa5.Location = new System.Drawing.Point(210, 152);
            this.pa5.Margin = new System.Windows.Forms.Padding(4);
            this.pa5.MaxLength = 3;
            this.pa5.Name = "pa5";
            this.pa5.Size = new System.Drawing.Size(40, 22);
            this.pa5.TabIndex = 21;
            // 
            // pa6
            // 
            this.pa6.Location = new System.Drawing.Point(210, 184);
            this.pa6.Margin = new System.Windows.Forms.Padding(4);
            this.pa6.MaxLength = 3;
            this.pa6.Name = "pa6";
            this.pa6.Size = new System.Drawing.Size(40, 22);
            this.pa6.TabIndex = 26;
            // 
            // ps1
            // 
            this.ps1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ps1.FormattingEnabled = true;
            this.ps1.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.ps1.Location = new System.Drawing.Point(259, 24);
            this.ps1.Margin = new System.Windows.Forms.Padding(4);
            this.ps1.Name = "ps1";
            this.ps1.Size = new System.Drawing.Size(93, 24);
            this.ps1.TabIndex = 2;
            // 
            // ps2
            // 
            this.ps2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ps2.FormattingEnabled = true;
            this.ps2.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.ps2.Location = new System.Drawing.Point(259, 56);
            this.ps2.Margin = new System.Windows.Forms.Padding(4);
            this.ps2.Name = "ps2";
            this.ps2.Size = new System.Drawing.Size(93, 24);
            this.ps2.TabIndex = 7;
            // 
            // ps3
            // 
            this.ps3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ps3.FormattingEnabled = true;
            this.ps3.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.ps3.Location = new System.Drawing.Point(259, 88);
            this.ps3.Margin = new System.Windows.Forms.Padding(4);
            this.ps3.Name = "ps3";
            this.ps3.Size = new System.Drawing.Size(93, 24);
            this.ps3.TabIndex = 12;
            // 
            // ps4
            // 
            this.ps4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ps4.FormattingEnabled = true;
            this.ps4.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.ps4.Location = new System.Drawing.Point(259, 120);
            this.ps4.Margin = new System.Windows.Forms.Padding(4);
            this.ps4.Name = "ps4";
            this.ps4.Size = new System.Drawing.Size(93, 24);
            this.ps4.TabIndex = 17;
            // 
            // ps5
            // 
            this.ps5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ps5.FormattingEnabled = true;
            this.ps5.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.ps5.Location = new System.Drawing.Point(259, 152);
            this.ps5.Margin = new System.Windows.Forms.Padding(4);
            this.ps5.Name = "ps5";
            this.ps5.Size = new System.Drawing.Size(93, 24);
            this.ps5.TabIndex = 22;
            // 
            // ps6
            // 
            this.ps6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ps6.FormattingEnabled = true;
            this.ps6.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.ps6.Location = new System.Drawing.Point(259, 184);
            this.ps6.Margin = new System.Windows.Forms.Padding(4);
            this.ps6.Name = "ps6";
            this.ps6.Size = new System.Drawing.Size(93, 24);
            this.ps6.TabIndex = 27;
            // 
            // pb1
            // 
            this.pb1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pb1.FormattingEnabled = true;
            this.pb1.Location = new System.Drawing.Point(362, 24);
            this.pb1.Margin = new System.Windows.Forms.Padding(4);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(148, 24);
            this.pb1.TabIndex = 3;
            // 
            // pb2
            // 
            this.pb2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pb2.FormattingEnabled = true;
            this.pb2.Location = new System.Drawing.Point(362, 56);
            this.pb2.Margin = new System.Windows.Forms.Padding(4);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(148, 24);
            this.pb2.TabIndex = 8;
            // 
            // pb3
            // 
            this.pb3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pb3.FormattingEnabled = true;
            this.pb3.Location = new System.Drawing.Point(362, 88);
            this.pb3.Margin = new System.Windows.Forms.Padding(4);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(148, 24);
            this.pb3.TabIndex = 13;
            // 
            // pb4
            // 
            this.pb4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pb4.FormattingEnabled = true;
            this.pb4.Location = new System.Drawing.Point(362, 120);
            this.pb4.Margin = new System.Windows.Forms.Padding(4);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(148, 24);
            this.pb4.TabIndex = 18;
            // 
            // pb5
            // 
            this.pb5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pb5.FormattingEnabled = true;
            this.pb5.Location = new System.Drawing.Point(362, 152);
            this.pb5.Margin = new System.Windows.Forms.Padding(4);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(148, 24);
            this.pb5.TabIndex = 23;
            // 
            // pb6
            // 
            this.pb6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pb6.FormattingEnabled = true;
            this.pb6.Location = new System.Drawing.Point(362, 184);
            this.pb6.Margin = new System.Windows.Forms.Padding(4);
            this.pb6.Name = "pb6";
            this.pb6.Size = new System.Drawing.Size(148, 24);
            this.pb6.TabIndex = 28;
            // 
            // psc1
            // 
            this.psc1.AutoSize = true;
            this.psc1.Location = new System.Drawing.Point(548, 24);
            this.psc1.Margin = new System.Windows.Forms.Padding(33, 4, 4, 4);
            this.psc1.Name = "psc1";
            this.psc1.Size = new System.Drawing.Size(18, 17);
            this.psc1.TabIndex = 4;
            this.psc1.UseVisualStyleBackColor = true;
            // 
            // psc2
            // 
            this.psc2.AutoSize = true;
            this.psc2.Location = new System.Drawing.Point(548, 56);
            this.psc2.Margin = new System.Windows.Forms.Padding(33, 4, 4, 4);
            this.psc2.Name = "psc2";
            this.psc2.Size = new System.Drawing.Size(18, 17);
            this.psc2.TabIndex = 9;
            this.psc2.UseVisualStyleBackColor = true;
            // 
            // psc3
            // 
            this.psc3.AutoSize = true;
            this.psc3.Location = new System.Drawing.Point(548, 88);
            this.psc3.Margin = new System.Windows.Forms.Padding(33, 4, 4, 4);
            this.psc3.Name = "psc3";
            this.psc3.Size = new System.Drawing.Size(18, 17);
            this.psc3.TabIndex = 14;
            this.psc3.UseVisualStyleBackColor = true;
            // 
            // psc4
            // 
            this.psc4.AutoSize = true;
            this.psc4.Location = new System.Drawing.Point(548, 120);
            this.psc4.Margin = new System.Windows.Forms.Padding(33, 4, 4, 4);
            this.psc4.Name = "psc4";
            this.psc4.Size = new System.Drawing.Size(18, 17);
            this.psc4.TabIndex = 19;
            this.psc4.UseVisualStyleBackColor = true;
            // 
            // psc5
            // 
            this.psc5.AutoSize = true;
            this.psc5.Location = new System.Drawing.Point(548, 152);
            this.psc5.Margin = new System.Windows.Forms.Padding(33, 4, 4, 4);
            this.psc5.Name = "psc5";
            this.psc5.Size = new System.Drawing.Size(18, 17);
            this.psc5.TabIndex = 24;
            this.psc5.UseVisualStyleBackColor = true;
            // 
            // psc6
            // 
            this.psc6.AutoSize = true;
            this.psc6.Location = new System.Drawing.Point(548, 184);
            this.psc6.Margin = new System.Windows.Forms.Padding(33, 4, 4, 4);
            this.psc6.Name = "psc6";
            this.psc6.Size = new System.Drawing.Size(18, 17);
            this.psc6.TabIndex = 29;
            this.psc6.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(24, 459);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(174, 22);
            this.label11.TabIndex = 0;
            this.label11.Text = "Child Passenger Details";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 676);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Mobile Number :      +91";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 193);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 22);
            this.label9.TabIndex = 0;
            this.label9.Text = "Passenger Details";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.90558F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.09442F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 293F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.stationfrom, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.trainno, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.stationto, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.bdpt, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.classcode, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.gn, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ck, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.ld, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.jdate, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 49);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(621, 127);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "From :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 36);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Date :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 68);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 34);
            this.label4.TabIndex = 0;
            this.label4.Text = "Train No :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 108);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Quota :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(206, 6);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "To :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(206, 68);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Bdg Pt :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 36);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 6, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Class :";
            // 
            // stationfrom
            // 
            this.stationfrom.Location = new System.Drawing.Point(72, 4);
            this.stationfrom.Margin = new System.Windows.Forms.Padding(4);
            this.stationfrom.Name = "stationfrom";
            this.stationfrom.Size = new System.Drawing.Size(126, 22);
            this.stationfrom.TabIndex = 0;
            this.stationfrom.Leave += new System.EventHandler(this.focusfrom);
            // 
            // trainno
            // 
            this.trainno.Location = new System.Drawing.Point(72, 66);
            this.trainno.Margin = new System.Windows.Forms.Padding(4);
            this.trainno.Name = "trainno";
            this.trainno.Size = new System.Drawing.Size(126, 22);
            this.trainno.TabIndex = 4;
            // 
            // stationto
            // 
            this.stationto.Location = new System.Drawing.Point(331, 4);
            this.stationto.Margin = new System.Windows.Forms.Padding(4);
            this.stationto.Name = "stationto";
            this.stationto.Size = new System.Drawing.Size(132, 22);
            this.stationto.TabIndex = 1;
            this.stationto.Leave += new System.EventHandler(this.focusto);
            // 
            // bdpt
            // 
            this.bdpt.Location = new System.Drawing.Point(331, 66);
            this.bdpt.Margin = new System.Windows.Forms.Padding(4);
            this.bdpt.Name = "bdpt";
            this.bdpt.Size = new System.Drawing.Size(132, 22);
            this.bdpt.TabIndex = 5;
            // 
            // classcode
            // 
            this.classcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classcode.FormattingEnabled = true;
            this.classcode.Items.AddRange(new object[] {
            "First Class AC(1A)",
            "First Class(FC)",
            "AC 2-tier sleeper(2A)",
            "AC 3 Tier(3A)",
            "AC chair Car(CC)",
            "Sleeper Class(SL)",
            "Second Sitting(2S)",
            "AC 3 Tier Economy(3E)"});
            this.classcode.Location = new System.Drawing.Point(331, 34);
            this.classcode.Margin = new System.Windows.Forms.Padding(4);
            this.classcode.Name = "classcode";
            this.classcode.Size = new System.Drawing.Size(160, 24);
            this.classcode.TabIndex = 3;
            this.classcode.SelectedIndexChanged += new System.EventHandler(this.classcode_SelectedIndexChanged);
            // 
            // gn
            // 
            this.gn.AutoSize = true;
            this.gn.Location = new System.Drawing.Point(72, 106);
            this.gn.Margin = new System.Windows.Forms.Padding(4);
            this.gn.Name = "gn";
            this.gn.Size = new System.Drawing.Size(80, 21);
            this.gn.TabIndex = 6;
            this.gn.TabStop = true;
            this.gn.Text = "General";
            this.gn.UseVisualStyleBackColor = true;
            // 
            // ck
            // 
            this.ck.AutoSize = true;
            this.ck.Location = new System.Drawing.Point(206, 106);
            this.ck.Margin = new System.Windows.Forms.Padding(4);
            this.ck.Name = "ck";
            this.ck.Size = new System.Drawing.Size(68, 21);
            this.ck.TabIndex = 7;
            this.ck.TabStop = true;
            this.ck.Text = "Tatkal";
            this.ck.UseVisualStyleBackColor = true;
            // 
            // ld
            // 
            this.ld.AutoSize = true;
            this.ld.Location = new System.Drawing.Point(331, 106);
            this.ld.Margin = new System.Windows.Forms.Padding(4);
            this.ld.Name = "ld";
            this.ld.Size = new System.Drawing.Size(71, 21);
            this.ld.TabIndex = 7;
            this.ld.TabStop = true;
            this.ld.Text = "Ladies";
            this.ld.UseVisualStyleBackColor = true;
            // 
            // jdate
            // 
            this.jdate.CustomFormat = "dd/MM/yyyy";
            this.jdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.jdate.Location = new System.Drawing.Point(72, 34);
            this.jdate.Margin = new System.Windows.Forms.Padding(4);
            this.jdate.Name = "jdate";
            this.jdate.Size = new System.Drawing.Size(126, 22);
            this.jdate.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(200, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please Fill The Details Below..";
            // 
            // edittick
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 569);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "edittick";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Edit Ticket";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label paymenttype;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox pay;
        public System.Windows.Forms.TextBox irctcp;
        public System.Windows.Forms.TextBox irctcu;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox mno;
        public System.Windows.Forms.CheckBox cfau;
        public System.Windows.Forms.TextBox cpn1;
        public System.Windows.Forms.TextBox cpn2;
        public System.Windows.Forms.ComboBox cpa1;
        public System.Windows.Forms.ComboBox cpa2;
        public System.Windows.Forms.ComboBox cps2;
        public System.Windows.Forms.ComboBox cps1;
        public System.Windows.Forms.TextBox pn1;
        public System.Windows.Forms.TextBox pn3;
        public System.Windows.Forms.TextBox pn2;
        public System.Windows.Forms.TextBox pn4;
        public System.Windows.Forms.TextBox pn5;
        public System.Windows.Forms.TextBox pn6;
        public System.Windows.Forms.TextBox pa1;
        public System.Windows.Forms.TextBox pa2;
        public System.Windows.Forms.TextBox pa3;
        public System.Windows.Forms.TextBox pa4;
        public System.Windows.Forms.TextBox pa5;
        public System.Windows.Forms.TextBox pa6;
        public System.Windows.Forms.ComboBox ps1;
        public System.Windows.Forms.ComboBox ps2;
        public System.Windows.Forms.ComboBox pb1;
        public System.Windows.Forms.ComboBox pb2;
        public System.Windows.Forms.ComboBox pb3;
        public System.Windows.Forms.ComboBox pb4;
        public System.Windows.Forms.ComboBox pb5;
        public System.Windows.Forms.ComboBox pb6;
        public System.Windows.Forms.CheckBox psc1;
        public System.Windows.Forms.CheckBox psc2;
        public System.Windows.Forms.CheckBox psc3;
        public System.Windows.Forms.CheckBox psc4;
        public System.Windows.Forms.CheckBox psc5;
        public System.Windows.Forms.CheckBox psc6;
        public System.Windows.Forms.TextBox stationfrom;
        public System.Windows.Forms.TextBox trainno;
        public System.Windows.Forms.TextBox stationto;
        public System.Windows.Forms.TextBox bdpt;
        public System.Windows.Forms.ComboBox classcode;
        public System.Windows.Forms.RadioButton gn;
        public System.Windows.Forms.RadioButton ck;
        public System.Windows.Forms.RadioButton ld;
        public System.Windows.Forms.DateTimePicker jdate;
        public System.Windows.Forms.ComboBox ps3;
        public System.Windows.Forms.ComboBox ps4;
        public System.Windows.Forms.ComboBox ps5;
        public System.Windows.Forms.ComboBox ps6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        public System.Windows.Forms.TextBox rstationfrom;
        public System.Windows.Forms.TextBox rtrainno;
        public System.Windows.Forms.TextBox rstationto;
        public System.Windows.Forms.TextBox rbdpt;
        public System.Windows.Forms.ComboBox rclasscode;
        public System.Windows.Forms.DateTimePicker rjdate;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label37;
        public System.Windows.Forms.ComboBox pictype1;
        public System.Windows.Forms.TextBox picard1;
        public System.Windows.Forms.ComboBox pictype2;
        public System.Windows.Forms.ComboBox pictype4;
        public System.Windows.Forms.ComboBox pictype3;
        public System.Windows.Forms.TextBox picard2;
        public System.Windows.Forms.TextBox picard3;
        public System.Windows.Forms.TextBox picard4;
    }
}