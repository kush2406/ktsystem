﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace kTsystem
{
    static class log
    {
        static public string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes
                  = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue
                  = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }
        public static void store(string tname, string eventd, string descd)
        {
            string all;
            try
            {
                if (staticstore.con == 0)
                {
                    Random r = new Random();
                    staticstore.con = r.Next(1, 10);
                }
                if (!Directory.Exists("applog"))
                {
                    Directory.CreateDirectory("applog");
                }
                string fname = @"applog\applog_" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_" + tname + "-" + staticstore.con.ToString() + ".log";
                string time = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
                StreamWriter sw = File.AppendText(fname);
                all = "##::##::" + time + "::" + eventd + "==" + descd;
                all = EncodeTo64(all);
                sw.WriteLine(all + "\n\r");
                sw.Close();
            }
            catch
            {
            }
        }
    }
}
