﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace kTsystem
{
    static class staticstore
    {
        public static System.Drawing.Image img;
        public static Form1 f1;
        public static string editf;
        public static Boolean addtick;
        public static bookwin bw;
        /*Details of Ticket*/
        public static string ticketname;
        public static string status;
        public static string ustatus;//temp
        public static string cstatus;
        public static string bstatus;
        //public static httpreq hp1;
        //public static httpreq hp2;
        //public static httpreq hp3;
        public static CookieContainer cc;
        //public static httpreq[] hp = new httpreq[10];

        static public string stationfrom;
        static public string stationto;
        static public string jdate;
        static public string jjdate;
        static public string day;
        static public string month;
        static public string year;
        static public string classcode;
        static public string sclasscode;
        static public string trainno;
        static public string bdgpt;
        static public string qt;
        static public string pn1;
        static public string pa1;
        static public string ps1;
        static public string pb1;
        static public string psc1;
        static public string pictype1;
        static public string picard1;
        static public string pn2;
        static public string pa2;
        static public string ps2;
        static public string pb2;
        static public string psc2;
        static public string pictype2;
        static public string picard2;
        static public string pn3;
        static public string pa3;
        static public string ps3;
        static public string pb3;
        static public string psc3;
        static public string pictype3;
        static public string picard3;
        static public string pn4;
        static public string pa4;
        static public string ps4;
        static public string pb4;
        static public string psc4;
        static public string pictype4;
        static public string picard4;
        static public string pn5;
        static public string pa5;
        static public string ps5;
        static public string pb5;
        static public string psc5;
        static public string pn6;
        static public string pa6;
        static public string ps6;
        static public string pb6;
        static public string psc6;
        static public string cpn1;
        static public string cpa1;
        static public string cps1;
        static public string cpn2;
        static public string cpa2;
        static public string cps2;
        static public string cfau;
        static public string payment;
        static public string mobno;
        static public string irctcu;
        static public string irctcp;
        static public string pay1;
        static public string pay2;
        static public string pay3;
        static public string notstntrn;
        static public string dynamicflg;
        static public string servtax;
        static public string tfare;
        static public string pxyip;
        static public string pxypo;
        static public int delay;

        static public string rstationfrom;
        static public string rstationto;
        static public string rjdate;
        static public string rjjdate;
        static public string rday;
        static public string rmonth;
        static public string ryear;
        static public string rclasscode;
        static public string rsclasscode;
        static public string rtrainno;
        static public string rbdgpt;
        /*Ticket Details Ends Here*/


        /*Details after adding Ticket*/
        static public int statustill;
        static public Boolean efb;
        static public Boolean at8;
        static public Boolean cp;
        static public string bvs;
        static public string bve;
        static public int retry;
        static public int submitclick;
        static public int booktype;
        /*Other Details at runtime*/
        static public string currentdate;
        static public string currentmonth;
        static public string currentyear;
        static public string timedate;

        static public string allClasses;
        static public string trainName;
        static public string trainType;
        static public string departure;
        static public string arrival;
        static public string runsOn;
        static public string typ;



        static public string rallClasses;
        static public string rtrainName;
        static public string rtrainType;
        static public string rdeparture;
        static public string rarrival;
        static public string rrunsOn;
        static public string rtyp;
        static public string rbankpost;


        static public string cap;
        static public string cost;
        static public string tatkalop;
        static public string bankrurl;
        static public string garib;
        static public string fp;
        static public bool closebr = true;

        static public string resultpay1;
        static public string resultpay2;
        static public string result1;
        static public string result2;
        static public string result3;
        static public string result4;
        static public string sbiresult0;
        static public string sbiresult1;
        static public string sbiresult2;
        static public string ubiresult1;
        static public string ubiresult2;
        static public string ubiresult3;
        static public string idbiresult1;
        static public string idbiresult2;
        static public string payresult;
        //a constant
        static public int con;
        static public int date;
        static public int hourstart;
        static public int bktime;
        static public int ntime;
        static public int tscount = 0;
        static public int tfcount = 0;
        //for nt showing message date range outside reservation period

        static public string ndate;
        static public string ndate1;
        static public string avldet;
        static public string dtnc;
        static public string avlsts;
        static public string jppar;
        static public string trnid;
        static public string nresult;
        static public string nresult1;
        static public string nresult2;
        static public string frresult;
        static public string vstate;
        static public bool isnew = false;
        static public string selfetch = "A";
        static public string chkdate;
        static public bool firstcall;
    }
}
