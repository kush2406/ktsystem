﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace kTsystem
{
    public partial class Form1 : Form
    {
        public delegate void agr0();
        public delegate void agr1(string st);
        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
            staticstore.f1 = this;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Thread th = new Thread(new ThreadStart(first.firstrun));
            th.Start();
        }

        public void keydown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        public void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (productkey.verify(this.mainp.Controls["textbox2"].Text))
                {
                    System.IO.File.WriteAllText(@"pk.nfo", this.mainp.Controls["textbox2"].Text);
                    invokets.closemei();
                }
                else
                {
                    MessageBox.Show("Invalid Product Key");
                }
            }
            catch (Exception et)
            {
                log.store("default", "Errorinsystem.cs1.cs", et.Message);
            }
        }

    }
}
